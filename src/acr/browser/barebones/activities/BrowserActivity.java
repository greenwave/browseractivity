package acr.browser.barebones.activities;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import acr.browser.barebones.R;
import acr.browser.barebones.customwebview.CustomWebView;
import acr.browser.barebones.databases.DatabaseHandler;
import acr.browser.barebones.databases.SpaceTokenizer;
import acr.browser.barebones.utilities.BookmarkPageVariables;
import acr.browser.barebones.utilities.CustomHistoryList;
import acr.browser.barebones.utilities.CustomHistoryList.HistoryObject;
import acr.browser.barebones.utilities.FinalVariables;
import acr.browser.barebones.utilities.HistoryPageVariables;
import acr.browser.barebones.utilities.JavaScriptHandler;
import acr.browser.barebones.utilities.Utils;
import acr.browser.barebones.webviewclasses.CustomChromeClient;
import acr.browser.barebones.webviewclasses.CustomDownloadListener;
import acr.browser.barebones.webviewclasses.CustomWebViewClient;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Instrumentation;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteMisuseException;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient.CustomViewCallback;
import android.webkit.WebIconDatabase;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class BrowserActivity extends Activity implements OnTouchListener {

	public static class ClickHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			String url = null;
			url = msg.getData().getString("url");
			handleLongClickOnBookmarks(url, msg.arg1);
		}

	}

	static class Handle extends Handler {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 1: {
				Log.d("MohuChannels", "within message handler line 134: Loading url:> " + getUrl.getText().toString());
				currentTab.loadUrl(getUrl.getText().toString());
				break;
			}
			case 2: {
				// deleteTab(msg.arg1);
				break;
			}
			case 3: {
				currentTab.invalidate();
				break;
			}
			}
			super.handleMessage(msg);
		}

	}

	static class NewTabHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				//newTab(homepage, true);
				Log.d("MohuChannels", "Created new tab in the NewTabHandler.");
				currentTab.loadUrl(homepage);
			}
			super.handleMessage(msg);
		}

	}

	private static BrowserActivity ACTIVITY;

	private static int index = 0;

	// constants
	public static final int MAX_TABS = FinalVariables.MAX_TABS;

	public static final int MAX_BOOKMARKS = FinalVariables.MAX_BOOKMARKS;

	// variables

	public static final boolean PAID_VERSION = FinalVariables.PAID_VERSION;
	public static final String HOMEPAGE = FinalVariables.HOMEPAGE;
	public static final int API = FinalVariables.API;
	public static final String SEPARATOR = "\\|\\$\\|SEPARATOR\\|\\$\\|";

	public static boolean DEVICE_HAS_GPS = false;
	// semi constants
	public static Context CONTEXT;
	public static String SEARCH;

	public static List<Integer> tabList;
	// variables
	public static CustomWebView currentTab;
	public static TextView currentTabTitle;

	public static MultiAutoCompleteTextView getUrl;
	public static TextView[] urlTitle;
	public static ProgressBar browserProgress;
	public static CustomWebView[] main;
	public static Rect bounds;
	public static long timeTabPressed;
	public static boolean fullScreen;
	public static int[] tabOrder = new int[MAX_TABS];
	public static ValueCallback<Uri> mUploadMessage;
	public static ImageView refresh;
	public static ProgressBar progressBar;
	public static String defaultUser;
	public static Drawable webpageOther;
	public static Drawable incognitoPage;
	public static Drawable exitTab;
	public static long loadTime = 0;
	public static int currentId = 0;
	public static int height32;
	public static int height;
	public static int width;
	public static int pixels;
	public static int leftPad;
	public static int rightPad;
	public static int id;
	public static int tenPad;
	public static boolean isPhone = false;
	//@Mohu Evan Boucher: Changed initial value fullscreen to true, was false.
	public static boolean showFullScreen = true;
	public static boolean noStockBrowser = true;
	public static boolean gestures;
	public static SharedPreferences settings;
	public static SharedPreferences.Editor edit;
	public static String user;
	public static String[] memoryURL;
	public static String[] bUrl;
	public static String[] bTitle;
	public static String[] columns;
	public static String homepage;
	public static String[][] urlToLoad;
	public static FrameLayout background;
	public static RelativeLayout uBar;
	public static RelativeLayout screen;
	public static HorizontalScrollView tabScroll;
	public static Animation slideUp;
	public static Animation slideDown;
	public static Animation fadeOut;
	public static Animation fadeIn;
	public static CookieManager cookieManager;
	public static Uri bookmarks;
	public static Handler handler, browserHandler;
	public static DatabaseHandler historyHandler;
	public static Drawable inactive;
	public static Drawable active;
	public static LinearLayout tabLayout;
	
	//@Mohu Evan Boucher: Added favorite icon image button.
	public static ImageButton favicon;
	private boolean faviconOn = false;
	private static boolean urlBarVisible = false;
	/**
	 * True if the device is connected to the Internet.
	 * @Mohu Evan Boucher
	 */
	public static boolean pageConnected = true;
	/**
	 * True if we need to make first tab. False if first tab was already created.
	 * @Mohu Evan Boucher
	 */
	public static boolean createFirstTab = true;
	
	public static final String LICENSE_INTENT = "android.intent.action.LICENSE";
	
	private String cannotGoBackMessage = "You have no history to go back to.";
	
	private static CustomHistoryList historyList = new CustomHistoryList();
	
	private JavaScriptHandler MohuOneHandler;
	
	/**
	 * Set when the page is finished loading.
	 * @Mohu Evan Boucher
	 */
	private static  boolean pageFinishedLoading = false;
	
	/**
	 * Determines if the enter button has been hit on the url bar after being edited.
	 */
	private boolean enterHitOnUrl = true;
	
	private static ImageButton loadPageButton;
	

	/**
	 * This function will create a new Tab only at startup, and then replace the new tab with theURL string.
	 * This is meant so that the user never needs the URL bar.
	 * @param theUrl The url to replace the current tab with.
	 * @param display Wither to display the current view or not.
	 * @return
	 */
	public static int createTab(String theUrl, boolean display) {
		//@Mohu Evan: Changed to always replace the first tab with the opened URL.
		Log.d("MohuChannels", "Within CreateTab() theURL is::> " + theUrl);
		int id = 0;//-1
		/*
		 * @Mohu: Removed as we will never hit max tabs and will always use the first tab in the list (tab 0).
		for (int n = 0; n < MAX_TABS; n++) {
			if (main[n] == null) {
				id = n;
				break;
			}
		}
		if (id != -1) {
		*/
			if (tabList.size() > 0) {
				if (display) {
					if (API < 16) {
						currentTabTitle.setBackgroundDrawable(inactive);
					} else {
						currentTabTitle.setBackground(inactive);
					}
					currentTabTitle.setPadding(leftPad, 0, rightPad, 0);
				}
			}
			final TextView title = new TextView(CONTEXT);
			title.setText("New Tab");
			if (display) {
				if (API < 16) {
					title.setBackgroundDrawable(active);
				} else {
					title.setBackground(active);
				}
			} else {
				if (API < 16) {
					title.setBackgroundDrawable(inactive);
				} else {
					title.setBackground(inactive);
				}
			}
			title.setSingleLine(true);
			title.setGravity(Gravity.CENTER_VERTICAL);
			title.setHeight(height32);
			title.setWidth(pixels);
			title.setPadding(leftPad, 0, rightPad, 0);
			title.setId(id);
			title.setGravity(Gravity.CENTER_VERTICAL);

			title.setCompoundDrawables(webpageOther, null, exitTab, null);

			Drawable[] drawables = title.getCompoundDrawables();
			bounds = drawables[2].getBounds();
			title.setOnTouchListener(ACTIVITY);
			Animation holo = AnimationUtils.loadAnimation(CONTEXT, R.anim.up);
			tabLayout.addView(title);
			title.setVisibility(View.INVISIBLE);
			holo.setAnimationListener(new AnimationListener() {

				@Override
				public void onAnimationEnd(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationStart(Animation animation) {
					title.setVisibility(View.VISIBLE);
				}

			});
			title.startAnimation(holo);
			urlTitle[id] = title;

			urlTitle[id].setText("New Tab");

			if (theUrl != null) {
				main[id] = generateTab(id, theUrl, display);
				//@Mohu Evan Boucher: Added this debugging statements TODO REMOVE
				Log.d("MohuChannels", "The ID is: > " + id + "   Generated a new tab for the URL:::> " + theUrl + "    Within createTab().");
			} else {
				//@Mohu Evan Boucher: TODO Remove later.  Debugging statement for figuring out what is needed at startup. 
				//want to block new tabs.
				Log.d("MohuChannels", "Within createTab(). Generating a homepage tab.");
				main[id] = generateTab(id, homepage, display);
			}
		/*
		 * @Mohu Evan Boucher: Removed so that the same tab is always replaced. No new tab is created and the old tab is rewritten.
		} else {
			/*
			 * Instead of doing nothing, lets replace the current tab with the url we received.
			 * @Mohu Evan Boucher
			 
			Log.d("MohuChannels", "Lightning is trying to open a new tab, but we are replacing the current tab instead. URL is::> " + theUrl);
			currentTab.loadUrl(theUrl);
			//Utils.showToast(CONTEXT, "Max number of tabs reached");
		}
		*/
		return id;
	}
	
	
	public static void deleteBookmark(String url) {
		File book = new File(CONTEXT.getFilesDir(), "bookmarks");
		File bookUrl = new File(CONTEXT.getFilesDir(), "bookurl");
		int n = 0;
		try {
			BufferedWriter bookWriter = new BufferedWriter(new FileWriter(book));
			BufferedWriter urlWriter = new BufferedWriter(new FileWriter(
					bookUrl));
			while (bUrl[n] != null && n < (MAX_BOOKMARKS - 1)) {
				if (!bUrl[n].equalsIgnoreCase(url)) {
					bookWriter.write(bTitle[n]);
					urlWriter.write(bUrl[n]);
					bookWriter.newLine();
					urlWriter.newLine();
				}
				n++;
			}
			bookWriter.close();
			urlWriter.close();
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		}
		for (int p = 0; p < MAX_BOOKMARKS; p++) {
			bUrl[p] = null;
			bTitle[p] = null;
		}
		try {
			BufferedReader readBook = new BufferedReader(new FileReader(book));
			BufferedReader readUrl = new BufferedReader(new FileReader(bookUrl));
			String t, u;
			int z = 0;
			while ((t = readBook.readLine()) != null
					&& (u = readUrl.readLine()) != null && z < MAX_BOOKMARKS) {
				bUrl[z] = u;
				bTitle[z] = t;
				z++;
			}
			readBook.close();
			readUrl.close();
		} catch (IOException ignored) {
		}
		//@Mohu Evan Boucher: Commented this out so that deleting a tab does not bring you to the bookmarks page.
		//openBookmarks(CONTEXT, currentTab);
	}

	public static void generateHistory(final CustomWebView view,
			final Context context) {
		Thread history = new Thread(new Runnable() {

			@Override
			public void run() {
				String historyHtml = HistoryPageVariables.Heading;
				Cursor historyCursor = null;
				String[][] h = new String[50][3];

				try {
					SQLiteDatabase s = historyHandler.getReadableDatabase();
					historyCursor = s.query("history", // URI
														// of
							columns, // Which columns to return
							null, // Which rows to return (all rows)
							null, // Selection arguments (none)
							null, null, null);

					handler.sendEmptyMessage(1);

				} catch (SQLiteException ignored) {
				} catch (NullPointerException ignored) {
				} catch (IllegalStateException ignored) {
				}

				try {
					if (historyCursor != null) {
						if (historyCursor.moveToLast()) {
							// Variable for holding the retrieved URL
							int urlColumn = historyCursor.getColumnIndex("url");
							int titleColumn = historyCursor
									.getColumnIndex("title");
							// Reference to the the column containing the URL
							int n = 0;
							do {

								h[n][0] = historyCursor.getString(urlColumn);
								h[n][2] = h[n][0].substring(0,
										Math.min(100, h[n][0].length()))
										+ "...";
								h[n][1] = historyCursor.getString(titleColumn);
								historyHtml += (HistoryPageVariables.Part1
										+ h[n][0] + HistoryPageVariables.Part2
										+ h[n][1] + HistoryPageVariables.Part3
										+ h[n][2] + HistoryPageVariables.Part4);
								n++;
							} while (n < 49 && historyCursor.moveToPrevious());
						}
					}
				} catch (SQLiteException ignored) {
				} catch (NullPointerException ignored) {
				} catch (IllegalStateException ignored) {
				}

				historyHtml += BookmarkPageVariables.End;
				File historyWebPage = new File(context.getFilesDir(),
						"history.html");
				try {
					FileWriter hWriter = new FileWriter(historyWebPage, false);
					hWriter.write(historyHtml);
					hWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (uBar.isShown()) {
					currentTabTitle.setText("History");
					setUrlText("");
					getUrl.setPadding(tenPad, 0, tenPad, 0);
				}

				view.loadUrl("file://" + historyWebPage);
			}

		});
		history.run();
	}

	public static CustomWebView generateTab(final int pageToView, String Url,
			final boolean display) {
		Log.d("MohuChannels", "GenerateTab() The Url is ::> " + Url);
		
		CustomWebView view = new CustomWebView(CONTEXT);
		view.setId(pageToView);
		view.setWebViewClient(new CustomWebViewClient(ACTIVITY));
		view.setWebChromeClient(new CustomChromeClient(ACTIVITY));
		if (API > 8) {
			view.setDownloadListener(new CustomDownloadListener(ACTIVITY));
		}
		main[pageToView] = view;
		if (display) {
			if (currentId != -1) {
				background.removeView(currentTab);
			}
			background.addView(view);
			view.requestFocus();
			currentId = pageToView;
			currentTab = main[pageToView];
			currentTabTitle = urlTitle[pageToView];
		}
		uBar.bringToFront();
		if (Url.contains("about:home")) {
			goBookmarks(CONTEXT, view);
		} else if (Url.contains("about:blank")) {
			view.loadUrl("");
		} else {
			view.loadUrl(Url);
		}
		Log.d("MohuChannels", "generateTab at the end...The url is::> " + Url);
		Log.i("Browser", "tab complete");
		return view;
	}
	/**
	 * Replaces the current view in order to get the callback from the URL redirect.
	 * @Mohu Evan Boucher
	 * @param pageToView The index of the page to view
	 * @param Url The Url to load (empty string for redirects).
	 * @param display Boolean to display the view.
	 * @return
	 */
	public static CustomWebView generateCurrentView(final int pageToView, String Url,
			final boolean display) {
		Log.d("MohuChannels", "GenerateTab() The Url is ::> " + Url);
		
		CustomWebView view = currentTab; //new CustomWebView(CONTEXT);
		view.setId(pageToView);
		view.setWebViewClient(new CustomWebViewClient(ACTIVITY));
		view.setWebChromeClient(new CustomChromeClient(ACTIVITY));
		if (API > 8) {
			view.setDownloadListener(new CustomDownloadListener(ACTIVITY));
		}
		main[pageToView] = view;
		if (display) {
			if (currentId != -1) {
				background.removeView(currentTab);
			}
			background.addView(view);
			view.requestFocus();
			currentId = pageToView;
			currentTab = main[pageToView];
			currentTabTitle = urlTitle[pageToView];
		}
		uBar.bringToFront();
		if (Url.contains("about:home")) {
			goBookmarks(CONTEXT, view);
		} else if (Url.contains("about:blank")) {
			view.loadUrl("");
		} else {
			view.loadUrl(Url);
		}
		Log.d("MohuChannels", "generateTab at the end...The url is::> " + Url);
		Log.i("Browser", "tab complete");
		return view;
	}

	public static String[] getArray(String input) {
		return input.split(SEPARATOR);
	}

	public static void goBack(CustomWebView view) {
		if (view.isShown() && view.canGoBack() && gestures) {
			view.goBack();
		}
		Animation left = AnimationUtils.loadAnimation(CONTEXT, R.anim.left);
		background.startAnimation(left);

	}

	static List<Map<String, String>> getBookmarks() {
		List<Map<String, String>> bookmarks = new ArrayList<Map<String, String>>();
		File bookUrl = new File(CONTEXT.getFilesDir(), "bookurl");
		File book = new File(CONTEXT.getFilesDir(), "bookmarks");
		try {
			BufferedReader readUrl = new BufferedReader(new FileReader(bookUrl));
			BufferedReader readBook = new BufferedReader(new FileReader(book));
			String u, t;
			while ((u = readUrl.readLine()) != null
					&& (t = readBook.readLine()) != null) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("title", '\u2605' + " " + t);
				map.put("url", u);
				bookmarks.add(map);
			}
			readBook.close();
			readUrl.close();
		} catch (FileNotFoundException ignored) {
		} catch (IOException ignored) {
		}
		return bookmarks;
	}

	static void goBookmarks(Context context, CustomWebView view) {
		//@Mohu Evan Boucher: Added for debugging and finding bookmarks path/directory.
		Log.d("MohuChannels", "The value of context.getFilesDir()::> " + context.getFilesDir().getPath());
		File book = new File(context.getFilesDir(), "bookmarks");
		File bookUrl = new File(context.getFilesDir(), "bookurl");
		try {
			BufferedReader readBook = new BufferedReader(new FileReader(book));
			BufferedReader readUrl = new BufferedReader(new FileReader(bookUrl));
			String t, u;
			int n = 0;
			while ((t = readBook.readLine()) != null
					&& (u = readUrl.readLine()) != null && n < MAX_BOOKMARKS) {
				bUrl[n] = u;
				bTitle[n] = t;

				n++;
			}
			readBook.close();
			readUrl.close();
		} catch (FileNotFoundException ignored) {
		} catch (IOException ignored) {
		}
		openBookmarks(context, view);
	}

	public static void goForward(CustomWebView view) {
		if (view.isShown() && view.canGoForward() && gestures) {
			view.goForward();
		}
		Animation right = AnimationUtils.loadAnimation(CONTEXT, R.anim.right);
		background.startAnimation(right);
	}

	public static void handleLongClickOnBookmarks(final String clickedURL,
			final int n) {
		if (clickedURL != null) {

			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					switch (which) {
					case DialogInterface.BUTTON_POSITIVE: {
						renameBookmark(clickedURL);
						break;
					}
					case DialogInterface.BUTTON_NEGATIVE: {
						main[n].loadUrl(clickedURL);
						break;
					}
					case DialogInterface.BUTTON_NEUTRAL: {
						deleteBookmark(clickedURL);
						break;
					}
					}
				}
			};

			AlertDialog.Builder builder = new AlertDialog.Builder(CONTEXT); // dialog
			builder.setMessage("What would you like to do with this bookmark?")
					.setPositiveButton("Rename", dialogClickListener)
					.setNegativeButton("Open", dialogClickListener)
					.setNeutralButton("Delete", dialogClickListener).show();
		}
	}

	public static int newId() {

		Random n = new Random();
		int id = n.nextInt();

		while (tabList.contains(id)) {
			id = n.nextInt();
		}
		return id;
	}

	// new tab method, takes the id of the tab to be created and the url to load
	public static int newTab(final String theUrl, final boolean display) {
		/*
		 * @Mohu Evan: Commented this out so that a tab can never be created.
		 */
		//@Mohu Evan Boucher: Added if statement to catch initialization.
//		if (createFirstTab) {
			Log.i("Browser", "making tab");
			homepage = settings.getString("home", HOMEPAGE);
			Log.d("MohuChannels", "Within newTab(). The Url is::> " + theUrl);
			int finalID = createTab(theUrl, display);
			if (finalID != -1) {
				tabList.add(finalID);
				if (display) {
					currentId = finalID;
					currentTab = main[finalID];
					currentTabTitle = urlTitle[finalID];
				}
				//Set the createFirstTab variable to false so we cannot create any more tabs.
				createFirstTab = false;
				Log.d("MohuChannels", "The createFirstTab was set to false! Tab was craeted.");
				return finalID;
			} else {
				return 0;
			}
//		} else {
//			Log.d("MohuChannels", "First tab was already created, so just replace the currentTab within newTab().");
//			Log.d("MohuChannels", "The Url to load in newTab() is::> " + theUrl);
//			currentTab.loadUrl(theUrl);
//		}
//		return 0;
	}

	public static void onCreateWindow(Message resultMsg) {
		//@Mohu Evan Boucher: TODO Remove later.  Added debugging.
		Log.d("MohuChannels", "WIthin onCreateWindow() method. ");
		Log.d("MohuChannels", "The Url to load in onCreateWindow() is::> " + getUrl.getText().toString());
		//@Mohu Evan Boucher: Added if statement to catch initialization.
		//if (createFirstTab) {
			final WebBackForwardList tab_history = main[0].copyBackForwardList();
			Log.d("MohuChannels", "The length of the current history in onCreateWindow() is::> " + tab_history.getSize());
			newTab("", true);
			//generateTab(0, "", true);
		//}//else { //Maybe just comment out this code.
//			//Still need to test this.
//			Log.d("MohuChannels", "First tab was already created, so just replace the currentTab within onCreateWindow().");
//			Log.d("MohuChannels", "The Url to load in onCreateWindow() is::> " + getUrl.getText().toString());
//			//currentTab.loadUrl(getUrl.getText().toString());
//			generateCurrentView(0, "", true);
//		}
			//Added this later....
		//currentTab = main[0];
		//createTab("", true);
		
		//generateTab(0, "", true);
			Log.d("MohuChannels", "The length of the main web view array is::> " + main.length);
			WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
			//Log.d("MohuChannels", "After transport is set. value of it is:> " + transport.toString());
			transport.setWebView(currentTab);
			Log.d("MohuChannels", "After setting the transport view with the currentTab.");
			
			resultMsg.sendToTarget();
			Log.d("MohuChannels", "After the result is sent to the target in onCreateWindow().");
			Log.d("MohuChannels", "currentTab URL::> " + currentTab.getUrl());
			try {
				Log.d("MohuChannels", "Just before the browser handler in onCreateWindow. ::> " + getUrl.getText().toString());
				browserHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						Log.d("MohuChannels", "Return value of getUrl in onCreateWindow() ::> " + getUrl.getText().toString());
						main[0].loadUrl(getUrl.getText().toString());
						Log.d("MohuChannels", "About to bring the main[0] to front.");
						main[0].bringToFront();
						Log.d("MohuChannels", "The length of the main web view array is (End of onCreateWindow in BrowserActivity)::> " + main.length);
						currentTab.setCustomHistory(tab_history);
					}
				}, 500);
				Log.d("MohuChannels", "AFTER THE browser handler in onCreateWindow. ::> " + getUrl.getText().toString());
			} catch (Exception e) {
				Log.d("MohuChannels", "Caught an error in onCreateWindow when tring to run the runnable.");
				Log.d("MohuChannels", e.getMessage());
				e.printStackTrace();
			}

		
	}
	
	public static void onHideCustomView(FrameLayout fullScreenContainer,
			CustomViewCallback mCustomViewCallback, int orientation) {
		if (!settings.getBoolean("hidestatus", false)) {
			ACTIVITY.getWindow().clearFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
		FrameLayout screen = (FrameLayout) ACTIVITY.getWindow().getDecorView();
		screen.removeView(fullScreenContainer);
		fullScreenContainer = null;
		mCustomViewCallback.onCustomViewHidden();
		ACTIVITY.setRequestedOrientation(orientation);
		background.addView(currentTab);
		uBar.setVisibility(View.VISIBLE);
		uBar.bringToFront();
	}

	private static Message click;

	public static boolean onLongClick() {
		int n = currentId;
		if (currentId == -1 || currentTab == null) {
			return true;
		}
		final HitTestResult result = currentTab.getHitTestResult();
		if (currentTab.getUrl().contains(
				"file://" + CONTEXT.getFilesDir() + "/bookmarks.html")) {
			click = new Message();
			click.arg1 = n;
			click.setTarget(new ClickHandler());
			currentTab.requestFocusNodeHref(click);

			return true;
		} else if (result != null) {
			if (result.getExtra() != null) {
				if (result.getType() == 5 && API > 8) {
					DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case DialogInterface.BUTTON_POSITIVE: {
								//@Mohu Evan Boucher: Commented out so that a new window could not be opened.
								//int num = currentId;
								//newTab(result.getExtra(), false);
								// urlTitle[num].performClick();
								//currentId = num;
								//currentTab = main[num];
								//currentTabTitle = urlTitle[num];
								break;
							}
							case DialogInterface.BUTTON_NEGATIVE: {
								currentTab.loadUrl(result.getExtra());
								break;
							}
							case DialogInterface.BUTTON_NEUTRAL: {
								if (API > 8) {
									String url = result.getExtra();

									Utils.downloadFile(CONTEXT, url, null, null);

								}
								break;
							}
							}
						}
					};

					AlertDialog.Builder builder = new AlertDialog.Builder(
							CONTEXT); // dialog
					builder.setMessage(
							"What would you like to do with this image?")
							.setPositiveButton("Open in New Tab",
									dialogClickListener)
							.setNegativeButton("Open Normally",
									dialogClickListener)
							.setNeutralButton("Download Image",
									dialogClickListener).show();

				} else {
					DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case DialogInterface.BUTTON_NEGATIVE: {
								currentTab.loadUrl(result.getExtra());
								break;
							}
							case DialogInterface.BUTTON_NEUTRAL: {
								if (API < 11) {
									android.text.ClipboardManager clipboard = (android.text.ClipboardManager) ACTIVITY
											.getSystemService(Context.CLIPBOARD_SERVICE);
									clipboard.setText(result.getExtra());
								} else {
									ClipboardManager clipboard = (ClipboardManager) ACTIVITY
											.getSystemService(CLIPBOARD_SERVICE);
									ClipData clip = ClipData.newPlainText(
											"label", result.getExtra());
									clipboard.setPrimaryClip(clip);
								}
								break;
							}
							}
						}
					};

					AlertDialog.Builder builder = new AlertDialog.Builder(
							CONTEXT); // dialog
					
					//@Mohu Evan Boucher: Change Positive Button to add URL to bookmarks.
					builder.setTitle(result.getExtra())
							.setMessage(
									"What do you want to do with this link?")
							.setNegativeButton("Open Normally",
									dialogClickListener)
							.setNeutralButton("Copy link", dialogClickListener)
							.show();
				}
			}
			return true;

		} else {
			return false;
		}
	}

	public static void onPageFinished(WebView view, String url) {
		if (view.isShown()) {
			view.invalidate();
			progressBar.setVisibility(View.GONE);
			refresh.setVisibility(View.VISIBLE);
			
			if (showFullScreen && uBar.isShown()) {
				//@Mohu Evan Boucher: FullScreen URLBar: Commented out to not change the bar.
				//uBar.startAnimation(slideUp);
			}
		}
		view.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		Log.i("Lightning", "Page Finished");
		loadTime = System.currentTimeMillis() - loadTime;
		Log.i("Lightning", "Load Time: " + loadTime);
		
		//@Mohu Evan Boucher: This gives me every address (some twice), which causes issues with going back.
		Log.d("MohuChannels", "onPageFinished(): The page finished loading:> " + url);
		setUrlText(url); //@Mohu Evan Boucher; Added this so the url text view is always up to date.
		/*
		 * @Mohu Evan Boucher: Added to check that our last bookmark was accurate (seems to mess up with slow redirects).
		 * If the current pages url does not match our current index, we need to replace it.
		 */
//		Log.d("MohuGoogle", "onPageFinished(): The url contains google:> " + url.contains("www.google.com"));
//		Log.d("MohuGoogle", "onPageFinished(): The currentTab check if not dirty is:> " + !currentTab.checkIfNavWasPressed());
//		Log.d("MohuGoogle", "onPageFinished(): The historyList cur index url does not equal finished url is:> " + !(historyList.getCurIndex().url.equalsIgnoreCase(url)));
		if (url.contains("www.google.com") && !currentTab.checkIfNavWasPressed() && !(historyList.getCurIndex().url.equalsIgnoreCase(url))) {
			Log.d("MohuGoogle", "WE ARE REPLACING THE CURRENT INDEX!!!!!!!!!!!!!!!!!!@@@@@@@@@@");
			historyList.replaceCurIndex(url);
			Log.d("MohuGoogle", "<<<<>>>>><<>>>onPageFinished(): The list of history Url is: \n" + historyList.listHistory());
		}
		//pageFinishedLoading = true;
		// */
		
	}

	private static int numberPage;

	public static void onPageStarted(WebView view, String url, Bitmap favicon_bm) {
		Log.i("Lightning", "Page Started");
		loadTime = System.currentTimeMillis();
		numberPage = view.getId();

		if (url.startsWith("file://")) {
			view.getSettings().setUseWideViewPort(false);
		} else {
			view.getSettings().setUseWideViewPort(
					settings.getBoolean("wideviewport", true));
		}
		Log.d("MohuHistory", "onPageStarted(): just before view.isShown().");
		if (view.isShown()) {
			refresh.setVisibility(View.INVISIBLE);
			progressBar.setVisibility(View.VISIBLE);
			setUrlText(url);
			//@Mohu Evan Boucher: TODO This is where we can get the history URL for each custom page. (Ad page calls this 3 times...)
			//Log.d("MohuChannels", "onPageStarted(): The current Url is:> " + url);
			//@Mohu Evan Boucher: when the new tab is selected, change the favicon to match if it is book marked or not.
			setViewFavIcon(favicon);
		}
		Log.d("MohuHistory", "onPageStarted(): After the view.isShown() statement.");
		urlTitle[numberPage].setCompoundDrawables(webpageOther, null, exitTab,
				null);
		if (favicon_bm != null) {
			setFavicon(view.getId(), favicon_bm);
		}
		Log.d("MohuHistory", "onPageStarted(): The favicon_bm if statement");
		getUrl.setPadding(tenPad, 0, tenPad, 0);
		urlToLoad[numberPage][0] = url;
		Log.d("MohuHistory", "onPageStarted(): After urlToLoad is being set.");
		if (!uBar.isShown() && showFullScreen) {
			//@Mohu Evan Boucher: FullScreen URLBar: Commented out so uBar state does not change.
			//uBar.startAnimation(slideDown);
		}
		Log.d("MohuHistory", "onPageStarted(): End of onPageStarted function.");
	}
	
	//@Mohu Evan Boucher: Added function for changing the favIcon.
	public static void setViewFavIcon(ImageButton favIcon) {
		if (currentTab.faviconOn) {
			favIcon.setSelected(true);
		} else {
			favIcon.setSelected(false);
		}
	}
	
	
	public static void onProgressChanged(int id, int progress) {
		if (id == currentId) {
			browserProgress.setProgress(progress);
			if (progress < 100) {
				browserProgress.setVisibility(View.VISIBLE);
			} else {
				browserProgress.setVisibility(View.GONE);
			}
		}
	}

	public static void onReceivedTitle(int numberPage, String title) {
		Log.d("MohuHistory", "onReceivedTitle() Page Number: " + numberPage + ": Entered method. The title is:> " + title);
		if (title != null && title.length() != 0) {
			urlTitle[numberPage].setText(title);
			urlToLoad[numberPage][1] = title;
			Utils.updateHistory(CONTEXT, CONTEXT.getContentResolver(),
					noStockBrowser, urlToLoad[numberPage][0], title);
			/*
			 * @Mohu Evan Boucher: Added this to check our history list and see if back was pressed.
			 * This is so we do not re-add the same page (would be redundant and end in history deadlock).
			 */
			//Log.d("MohuGoogle", "onReceivedTitle(): The url for current tab is:> " + urlToLoad[numberPage][0]);
			Log.d("MohuHistory", "onReceivedTitle(): The url for current tab is:> " + urlToLoad[numberPage][0]);
//			if (old_title != null && title.equalsIgnoreCase(old_title)) {
//				Log.d("MohuHistory", "onReceivedTitle(): the old title and the new title are equal. NO ACTION!!!!!!");
//				return;
//			}
//			Log.d("MohuHistory", "The value of title equals Google:> " + title.contentEquals("Google"));
//			Log.d("mohuHistory", "onReceivedTitle(): The URL contains a google query string:> " + currentTab.getUrl().contains("#q="));
			/*
			 * This is a check for the google query string. For some reason when we load a google query. We get two titles from them.
			 * In order to not readd and lose current history we need to skip the first title we get from them.
			 * Not optimal, but the best solution I can find until I understand the onReceivedTitle function more.
			 * @Mohu Evan Boucher
			 */
			if (title.contentEquals("Google") && currentTab.getUrl().contains("&q=")) {
				Log.d("MohuHistory", "onReceivedTitle(): The title is a query page. NO ACTION!");
				return;
			}
			currentTab.checkBackPressed(urlToLoad[numberPage][0]);
			
		}
	}

	public static void onShowCustomView() {
		ACTIVITY.getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		background.removeView(currentTab);
		uBar.setVisibility(View.GONE);
	}

	static void openBookmarks(Context context, CustomWebView view) {
		String bookmarkHtml = BookmarkPageVariables.Heading;
		//@Mohu Evan Boucher: To see the path on the bookmarks page, uncomment this.
		//bookmarkHtml += "<div>The path is::> " + context.getFilesDir().getAbsolutePath() + " </div>";
		for (int n = 0; n < MAX_BOOKMARKS; n++) {
			if (bUrl[n] != null) {
				bookmarkHtml += (BookmarkPageVariables.Part1 + bUrl[n]
						+ BookmarkPageVariables.Part2 + bUrl[n]
						+ BookmarkPageVariables.Part3 + bTitle[n] + BookmarkPageVariables.Part4);
			}
		}
		bookmarkHtml += BookmarkPageVariables.End;
		File bookmarkWebPage = new File(context.getFilesDir(), "bookmarks.html");
		try {
			FileWriter bookWriter = new FileWriter(bookmarkWebPage, false);
			bookWriter.write(bookmarkHtml);
			bookWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		view.loadUrl("file://" + bookmarkWebPage);

		if (uBar.isShown()) {
			currentTabTitle.setText("Bookmarks");
			setUrlText("");
			getUrl.setPadding(tenPad, 0, tenPad, 0);
		}

	}

	public static void openFileChooser(ValueCallback<Uri> uploadMsg) {
		mUploadMessage = uploadMsg;
		Intent i = new Intent(Intent.ACTION_GET_CONTENT);
		i.addCategory(Intent.CATEGORY_OPENABLE);
		i.setType("*/*");
		ACTIVITY.startActivityForResult(
				Intent.createChooser(i, "File Chooser"), 1);
	}

	public static void reinitializeSettings() {
		int size = tabList.size();
		cookieManager = CookieManager.getInstance();
		CookieSyncManager.createInstance(CONTEXT);
		cookieManager.setAcceptCookie(settings.getBoolean("cookies", true));
		for (int n = 0; n < size; n++) {
			main[tabList.get(n)].settingsInitialization(CONTEXT);
		}
	}

	public static void renameBookmark(String url) {
		index = 0;
		for (int n = 0; n < MAX_BOOKMARKS; n++) {
			if (bUrl[n] != null) {
				if (bUrl[n].equalsIgnoreCase(url)) {
					index = n;
					break;
				}
			}
		}

		final AlertDialog.Builder homePicker = new AlertDialog.Builder(CONTEXT);
		homePicker.setTitle("Rename Bookmark");
		final EditText getText = new EditText(CONTEXT);
		getText.setText(bTitle[index]);

		homePicker.setView(getText);
		homePicker.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						bTitle[index] = getText.getText().toString();
						File book = new File(CONTEXT.getFilesDir(), "bookmarks");
						File bookUrl = new File(CONTEXT.getFilesDir(),
								"bookurl");
						int n = 0;
						try {
							BufferedWriter bookWriter = new BufferedWriter(
									new FileWriter(book));
							BufferedWriter urlWriter = new BufferedWriter(
									new FileWriter(bookUrl));
							while (bUrl[n] != null && n < (MAX_BOOKMARKS - 1)) {
								bookWriter.write(bTitle[n]);
								urlWriter.write(bUrl[n]);
								bookWriter.newLine();
								urlWriter.newLine();
								n++;
							}
							bookWriter.close();
							urlWriter.close();
						} catch (FileNotFoundException e) {
						} catch (IOException e) {
						}
						for (int p = 0; p < MAX_BOOKMARKS; p++) {
							bUrl[p] = null;
							bTitle[p] = null;
						}
						try {
							BufferedReader readBook = new BufferedReader(
									new FileReader(book));
							BufferedReader readUrl = new BufferedReader(
									new FileReader(bookUrl));
							String t, u;
							int z = 0;
							while ((t = readBook.readLine()) != null
									&& (u = readUrl.readLine()) != null
									&& z < MAX_BOOKMARKS) {
								bUrl[z] = u;
								bTitle[z] = t;
								z++;
							}
							readBook.close();
							readUrl.close();
						} catch (IOException ignored) {
						}
						openBookmarks(CONTEXT, currentTab);
					}
				});
		homePicker.show();

	}

	static void searchTheWeb(String query, Context context) {
		query = query.trim();
		currentTab.stopLoading();

		if (query.startsWith("www.")) {
			query = "http://" + query;
		} else if (query.startsWith("ftp.")) {
			query = "ftp://" + query;
		}

		boolean containsPeriod = query.contains(".");
		boolean isIPAddress = (TextUtils.isDigitsOnly(query.replace(".", "")) && (query
				.replace(".", "").length() >= 4));
		boolean aboutScheme = query.contains("about:");
		boolean validURL = (query.startsWith("ftp://")
				|| query.startsWith("http://") || query.startsWith("file://") || query
					.startsWith("https://")) || isIPAddress;
		boolean isSearch = ((query.contains(" ") || !containsPeriod) && !aboutScheme);

		if (query.contains("about:home") || query.contains("about:bookmarks")) {
			goBookmarks(context, currentTab);
		} else if (query.contains("about:history")) {
			generateHistory(currentTab, context);
		} else if (isSearch) {
			try {
				URLEncoder.encode(query, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			currentTab.loadUrl(SEARCH + query);
		} else if (!validURL) {
			currentTab.loadUrl("http://" + query);
		} else {
			currentTab.loadUrl(query);
		}
	}

	@SuppressWarnings("unused")
	public static void setFavicon(int id, Bitmap favicon) {
		Drawable icon;
		icon = new BitmapDrawable(null, favicon);
		icon.setBounds(0, 0, width / 2, height / 2);
		if (icon != null) {
			urlTitle[id].setCompoundDrawables(icon, null, exitTab, null);
		} else {
			urlTitle[id]
					.setCompoundDrawables(webpageOther, null, exitTab, null);
		}

	}

	public static void setUrlText(String url) {
		if (url != null) {
			if (!url.startsWith("file://")) {
				getUrl.setText(url);
			} else {
				getUrl.setText("");
			}
		}
	}

	static void share() {
		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);

		// set the type
		shareIntent.setType("text/plain");

		// add a subject
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				urlToLoad[currentId][1]);

		// build the body of the message to be shared
		String shareMessage = urlToLoad[currentId][0];

		// add the message
		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareMessage);

		// start the chooser for sharing
		CONTEXT.startActivity(Intent.createChooser(shareIntent,
				"Share this page"));
	}

	public static void toggleFullScreen() {
		Log.d("MohuChannels", "Called ToggleFullSCreen(). FullScreen:> " + fullScreen);
		showFullScreen = settings.getBoolean("fullscreen", true);
		CustomWebView.showFullScreen = showFullScreen;
		//if (fullScreen) {
			//background.removeView(uBar);
			//@Mohu Evan Boucher: Remove so that the Ubar is not added to the screen.
			//screen.addView(uBar);
			//fullScreen = false;
		//} else {
			/*
			screen.removeView(uBar);
			background.addView(uBar);
			fullScreen = true;
			*/
		//}
	}

	void back() {
		ImageView exit = (ImageView) findViewById(R.id.exit);
		exit.setBackgroundResource(R.drawable.button);
		if (isPhone) {
			RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
			relativeLayout1.removeView(exit);
		}
		exit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//@Mohu Evan Boucher: The onClick for the backButton. REMOVE LATER DEBUGGING
				Log.d("MohuHistory", "About to call canGoBack(). The value is:> " + currentTab.canGoBack());
				if (BrowserActivity.canGoBack()) { // use to be currentTab.canGoBack()
					currentTab.goBack();
				} else {
					//@Mohu Evan Boucher: We do not want the tab to exit ever.
					deleteTab(currentId);
					currentTab.onHideCustomView();
					/* Removed so the browser will hide instead of toast.
					WebBackForwardList tab_history = currentTab.copyBackForwardList();
					Log.d("MohuChannels", "The length of the current history in onback clicked is::> " + tab_history.getSize());
					Toast.makeText(CONTEXT, cannotGoBackMessage, Toast.LENGTH_LONG).show();
					*/
				}

			}

		});
		exit.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				//@Mohu Evan Boucher: Comment this out so that this does not close the application.
				clearCache();
				finish();
				currentTab.onHideCustomView();
				//Pass it up for now...
				/*
				WebBackForwardList tab_history = currentTab.copyBackForwardList();
				Log.d("MohuChannels", "The length of the current history in onLongClick Listener::> " + tab_history.getSize());
				Toast.makeText(CONTEXT, cannotGoBackMessage, Toast.LENGTH_LONG).show();
				*/
				return true;
			}

		});

	}

	void deleteTab(final int del) {
		if (API >= 11) {
			main[del].onPause();
		}
		Log.e("MohuChannels", "deleteTab() was called in BrowserActivity! THIS SHOULD NEVER HAPPEN!");
		main[del].stopLoading();
		main[del].clearHistory();
		tabScroll.smoothScrollTo(currentTabTitle.getLeft(), 0);
		edit.putString("oldPage", urlToLoad[del][0]);
		edit.commit();
		urlToLoad[del][0] = null;
		urlToLoad[del][1] = null;
		if (API < 16) {
			urlTitle[del].setBackgroundDrawable(active);
		} else {
			urlTitle[del].setBackground(active);
		}

		urlTitle[del].setPadding(leftPad, 0, rightPad, 0);
		Animation yolo = AnimationUtils.loadAnimation(this, R.anim.down);
		yolo.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation animation) {
				// urlTitle[del].setVisibility(View.GONE);
				tabLayout.post(new Runnable() {

					@Override
					public void run() {
						tabLayout.removeView(urlTitle[del]);
					}

				});
				findNewView(del);
				if (main[del] != null) {
					if (API > 11) {
						main[del].onPause();
					}
					if (main[del].isShown()) {
						background.removeView(main[del]);
					}
					main[del].removeAllViews();
					main[del] = null;
				}
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationStart(Animation animation) {
			}

		});
		urlTitle[del].startAnimation(yolo);
		uBar.bringToFront();
	}

	void enter() {
		getUrl.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View arg0, int arg1, KeyEvent arg2) {

				switch (arg1) {
				case KeyEvent.KEYCODE_ENTER:
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getUrl.getWindowToken(), 0);
					searchTheWeb(getUrl.getText().toString(), CONTEXT);
					//@Mohu Evan Boucher added to know when they edited the url bar.
					Log.d("MohuChannels", "setting enterHitOnUrl to true!!");
					enterHitOnUrl = true;
					return true;
				default:
					//@Mohu Evan Boucher added to know that they edited the url bar.
					Log.d("MohuChannels", "setting enter Url to false.");
					enterHitOnUrl = false;
					break;
				}
				return false;
			}

		});
		getUrl.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView arg0, int actionId,
					KeyEvent arg2) {
				if (actionId == EditorInfo.IME_ACTION_GO
						|| actionId == EditorInfo.IME_ACTION_DONE
						|| actionId == EditorInfo.IME_ACTION_NEXT
						|| actionId == EditorInfo.IME_ACTION_SEND
						|| actionId == EditorInfo.IME_ACTION_SEARCH
						|| (arg2.getAction() == KeyEvent.KEYCODE_ENTER)) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getUrl.getWindowToken(), 0);
					searchTheWeb(getUrl.getText().toString(), CONTEXT);
					return true;
				}
				return false;
			}

		});
	}

	@SuppressLint("HandlerLeak")
	void enterUrl() {
		getUrl = (MultiAutoCompleteTextView) findViewById(R.id.enterUrl);
		getUrl.setPadding(tenPad, 0, tenPad, 0);
		getUrl.setTextColor(getResources().getColor(android.R.color.black));
		getUrl.setPadding(tenPad, 0, tenPad, 0);
		getUrl.setBackgroundResource(R.drawable.book);
		getUrl.setPadding(tenPad, 0, tenPad, 0);
		final List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		List<Map<String, String>> books = getBookmarks();
		Iterator<Map<String, String>> it = books.iterator();
		while (it.hasNext()) {
			list.add(it.next());
		}
		handler = new Handler() {

			@Override
			public void handleMessage(Message msg) {

				switch (msg.what) {
				case 1: {
					SimpleAdapter adapter = new SimpleAdapter(CONTEXT, list,
							R.layout.two_line_autocomplete, new String[] {
									"title", "url" }, new int[] { R.id.title,
									R.id.url });

					getUrl.setAdapter(adapter);

					break;
				}
				case 2: {

					break;
				}
				}
			}
		};

		Thread updateAutoComplete = new Thread(new Runnable() {

			@Override
			public void run() {

				Cursor c = null;
				Cursor managedCursor = null;
				columns = new String[] { "url", "title" };
				try {

					bookmarks = Browser.BOOKMARKS_URI;
					c = getContentResolver().query(bookmarks, columns, null,
							null, null);
				} catch (SQLiteException ignored) {
				} catch (IllegalStateException ignored) {
				} catch (NullPointerException ignored) {
				}

				if (c != null) {
					noStockBrowser = false;
					Log.i("Browser", "detected AOSP browser");
				} else {
					noStockBrowser = true;
					Log.e("Browser", "did not detect AOSP browser");
				}
				if (c != null) {
					c.close();
				}
				try {

					managedCursor = null;
					SQLiteDatabase s = historyHandler.getReadableDatabase();
					managedCursor = s.query("history", // URI
														// of
							columns, // Which columns to return
							null, // Which rows to return (all rows)
							null, // Selection arguments (none)
							null, null, null);

				} catch (SQLiteException ignored) {
				} catch (NullPointerException ignored) {
				} catch (IllegalStateException ignored) {
				}

				try {
					if (managedCursor != null) {

						if (managedCursor.moveToLast()) {

							// Variable for holding the retrieved URL

							int urlColumn = managedCursor.getColumnIndex("url");
							int titleColumn = managedCursor
									.getColumnIndex("title");
							// Reference to the the column containing the URL
							do {
								String urlA = managedCursor
										.getString(urlColumn);
								String title = managedCursor
										.getString(titleColumn);
								Map<String, String> map = new HashMap<String, String>();
								map.put("title", title);
								map.put("url", urlA);
								list.add(map);
							} while (managedCursor.moveToPrevious());
						}
					}
					handler.sendEmptyMessage(1);
				} catch (SQLiteException ignored) {
				} catch (NullPointerException ignored) {
				} catch (IllegalStateException ignored) {
				}
				managedCursor.close();
			}

		});
		try {
			updateAutoComplete.start();
		} catch (NullPointerException ignored) {
		} catch (SQLiteMisuseException ignored) {
		} catch (IllegalStateException ignored) {
		}

		getUrl.setThreshold(1);
		getUrl.setTokenizer(new SpaceTokenizer());
		getUrl.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				try {
					String url;
					url = ((TextView) arg1.findViewById(R.id.url)).getText()
							.toString();
					getUrl.setText(url);
					searchTheWeb(url, CONTEXT);
					url = null;
					getUrl.setPadding(tenPad, 0, tenPad, 0);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(getUrl.getWindowToken(), 0);
				} catch (NullPointerException e) {
					Log.e("Browser Error: ",
							"NullPointerException on item click");
				}
			}

		});

		getUrl.setSelectAllOnFocus(true); // allows edittext to select all when
											// clicked
		
		/*
		 * Added to solve our URL loading and add to guide issues. This will check if the URL has been edited.
		 * If so, the add to guide button will change to load page. The new load page button will actually
		 * load the current URL in the text. Once the page loads, the button will change back to the 
		 * add to guide button.
		 * @Mohu Evan Boucher
		 */
		
		TextWatcher textWatcher = new TextWatcher() {
			  public void afterTextChanged(Editable s) {
				  Log.d("MohuChannels", "Within the text watcher after text edited. enterHitOnUrl is:> " + enterHitOnUrl);
				  //We now want to disable the add To Guide button until after Enter has been hit.
				  if (enterHitOnUrl) {
					  Log.d("MohuChannels", "Hiding Load Page Button and showing add to guide.^^");
					  //Change the load page button back to the add to guide button.
					  favicon.setVisibility(View.VISIBLE);
					  loadPageButton.setVisibility(View.GONE);
					  
					  return;
				  }
				  Log.d("MohuChannels", "Hiding add to guide button and showing loadPageButton.");
				  //They have edited the text so lets put the load the page button on. 
				  favicon.setVisibility(View.INVISIBLE);
				  loadPageButton.setVisibility(View.VISIBLE);
			  }

			  public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			  }

			  public void onTextChanged(CharSequence s, int start, int before,
			          int count) {

			  }
		};
		getUrl.addTextChangedListener(textWatcher);
	}

	void findNewView(int id) {
		int delete = tabList.indexOf(id);
		int leftId = id;
		boolean right = false, left = false;
		if (id == currentId) {
			
			if (main[id].isShown()) {
				main[id].startAnimation(fadeOut);
				background.removeView(main[id]);
				uBar.bringToFront();
			}

			if (tabList.size() > delete + 1) {
				id = tabList.get(delete + 1);
				if (urlTitle[id].isShown()) {
					background.addView(main[id]);
					main[id].setVisibility(View.VISIBLE);
					uBar.bringToFront();
					if (API < 16) {
						urlTitle[id].setBackgroundDrawable(active);
					} else {
						urlTitle[id].setBackground(active);
					}
					urlTitle[id].setPadding(leftPad, 0, rightPad, 0);
					currentId = id;
					currentTab = main[id];
					currentTabTitle = urlTitle[id];
					setUrlText(urlToLoad[currentId][0]);
					getUrl.setPadding(tenPad, 0, tenPad, 0);
					right = true;
					if (main[id].getProgress() < 100) {
						onProgressChanged(id, main[id].getProgress());
						refresh.setVisibility(View.INVISIBLE);
						progressBar.setVisibility(View.VISIBLE);
					} else {
						onProgressChanged(id, main[id].getProgress());
						progressBar.setVisibility(View.GONE);
						refresh.setVisibility(View.VISIBLE);
					}
					// break;
				}

			}
			if (!right) {
				if (delete > 0) {
					leftId = tabList.get(delete - 1);
					if (urlTitle[leftId].isShown()) {
						background.addView(main[leftId]);
						main[leftId].setVisibility(View.VISIBLE);
						// uBar.bringToFront();
						if (API < 16) {
							urlTitle[leftId].setBackgroundDrawable(active);
						} else {
							urlTitle[leftId].setBackground(active);
						}
						urlTitle[leftId].setPadding(leftPad, 0, rightPad, 0);
						currentId = leftId;
						currentTab = main[leftId];
						currentTabTitle = urlTitle[leftId];
						setUrlText(urlToLoad[currentId][0]);
						getUrl.setPadding(tenPad, 0, tenPad, 0);
						left = true;
						if (main[leftId].getProgress() < 100) {
							refresh.setVisibility(View.INVISIBLE);
							progressBar.setVisibility(View.VISIBLE);
							onProgressChanged(leftId,
									main[leftId].getProgress());
						} else {
							progressBar.setVisibility(View.GONE);
							refresh.setVisibility(View.VISIBLE);
							onProgressChanged(leftId,
									main[leftId].getProgress());
						}
						// break;
					}

				}

			}

		} else {
			right = left = true;
		}
		tabList.remove(delete);
		if (!(right || left)) {
			if (API > 11) {
				currentTab.onPause();
			}
			currentTab.pauseTimers();
			clearCache();
			currentTab = null;
			finish();
		}
		uBar.bringToFront();
		tabScroll.smoothScrollTo(currentTabTitle.getLeft(), 0);
	}

	public void clearCache() {
		if (settings.getBoolean("cache", false) && currentTab != null) {
			currentTab.clearCache(true);
			Log.i("Lightning", "Cache Cleared");

		}
		for (int n = 0; n < MAX_TABS; n++) {
			if (main[n] != null) {
				main[n].removeAllViews();
				main[n] = null;
			}
		}
	}

	@Override
	public void finish() {
		background.clearDisappearingChildren();
		background.removeView(currentTab);
		tabScroll.clearDisappearingChildren();
		super.finish();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub

		super.onDestroy();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub

		super.onStop();
	}

	void forward() {
		ImageView forward = (ImageView) findViewById(R.id.forward);
		forward.setBackgroundResource(R.drawable.button);
		if (isPhone) {
			RelativeLayout relativeLayout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
			relativeLayout1.removeView(forward);
		}
		forward.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (currentTab.canGoForward()) {
					currentTab.goForward();
				}
			}

		});
	}

	@SuppressLint({ "InlinedApi", "NewApi" })
	private void initialize() {
		//@Mohu Evan Boucher: Logging that ImageButton was successfully created.
		favicon = (ImageButton) findViewById(R.id.favicon);
		loadPageButton = (ImageButton) findViewById(R.id.loadpagebutton);
		loadPageButton.setVisibility(View.GONE);
		loadPageButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//Reload the browser with what is in the url.
				Log.d("MohuChannels", "Clicked the load page.");
				searchTheWeb(getUrl.getText().toString(), CONTEXT);
				enterHitOnUrl = true;
			}
		});
		
		tabList = new ArrayList<Integer>();
		bUrl = new String[MAX_BOOKMARKS];
		bTitle = new String[MAX_BOOKMARKS];
		main = new CustomWebView[MAX_TABS];
		urlTitle = new TextView[MAX_TABS];
		urlToLoad = new String[MAX_TABS][2];
		fullScreen = false;
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		historyHandler = new DatabaseHandler(this);
		cookieManager = CookieManager.getInstance();
		CookieSyncManager.createInstance(CONTEXT);
		cookieManager.setAcceptCookie(settings.getBoolean("cookies", true));

		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		browserProgress = (ProgressBar) findViewById(R.id.progressBar);
		browserProgress.setVisibility(View.GONE);

		if (API >= 11) {
			progressBar.setIndeterminateDrawable(getResources().getDrawable(
					R.drawable.ics_animation));
		} else {
			progressBar.setIndeterminateDrawable(getResources().getDrawable(
					R.drawable.ginger_animation));
		}
		//@Mohu Evan Boucher: Changed setting initialize fullscreen to true.
		showFullScreen = settings.getBoolean("fullscreen", true);
		uBar = (RelativeLayout) findViewById(R.id.urlBar);
		screen = (RelativeLayout) findViewById(R.id.background);
		slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
		slideDown = AnimationUtils.loadAnimation(this, R.anim.slide_down);
		//@Mohu Evan Boucher: NO current changes, but this is the animation for fading the browser in and out. 
		// We may be able to shorten this time to improve transition speed (Mark made a comment about this behavior).
		fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
		fadeOut.setDuration(250);
		fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
		// mShortAnimationDuration = getResources().getInteger(
		// android.R.integer.config_mediumAnimTime);
		slideUp.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation arg0) {
				uBar.setVisibility(View.GONE);
			}

			@Override
			public void onAnimationRepeat(Animation arg0) {

			}

			@Override
			public void onAnimationStart(Animation arg0) {

			}

		});
		slideDown.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationEnd(Animation animation) {

			}

			@Override
			public void onAnimationRepeat(Animation animation) {

			}

			@Override
			public void onAnimationStart(Animation animation) {
				
				uBar.setVisibility(View.VISIBLE);
			}

		});

		RelativeLayout refreshLayout = (RelativeLayout) findViewById(R.id.refreshLayout);
		refreshLayout.setBackgroundResource(R.drawable.button);

		// user agent
		if (API < 17) {
			user = new WebView(CONTEXT).getSettings().getUserAgentString();
		} else {
			user = WebSettings.getDefaultUserAgent(this);
		}

		background = (FrameLayout) findViewById(R.id.holder);
		defaultUser = user; // setting mobile user
		// agent
		switch (settings.getInt("search", 1)) {
		case 1:
			SEARCH = FinalVariables.GOOGLE_SEARCH;
			break;
		case 2:
			SEARCH = FinalVariables.BING_SEARCH;
			break;
		case 3:
			SEARCH = FinalVariables.YAHOO_SEARCH;
			break;
		case 4:
			SEARCH = FinalVariables.STARTPAGE_SEARCH;
			break;
		case 5:
			SEARCH = FinalVariables.DUCK_SEARCH;
			break;
		case 6:
			SEARCH = FinalVariables.BAIDU_SEARCH;
			break;
		case 7:
			SEARCH = FinalVariables.YANDEX_SEARCH;
			break;
		case 8:
			SEARCH = FinalVariables.DUCK_LITE_SEARCH;
			break;
		}

		exitTab = getResources().getDrawable(R.drawable.stop); // user
		// agent
		homepage = settings.getString("home", HOMEPAGE); // initializing
															// the
															// stored
															// homepage
															// variable

		gestures = settings.getBoolean("gestures", true);

		// initializing variables declared

		height = getResources().getDrawable(R.drawable.loading)
				.getMinimumHeight();
		width = getResources().getDrawable(R.drawable.loading)
				.getMinimumWidth();

		// hides keyboard so it doesn't default pop up
		this.getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

		// opens icondatabase so that favicons can be stored
		WebIconDatabase.getInstance().open(
				getDir("icons", MODE_PRIVATE).getPath());

		// scroll view containing tabs
		tabLayout = (LinearLayout) findViewById(R.id.tabLayout);
		tabScroll = (HorizontalScrollView) findViewById(R.id.tabScroll);
		tabScroll.setBackgroundColor(getResources().getColor(R.color.black));
		tabScroll.setHorizontalScrollBarEnabled(false);
		if (API > 8) {
			tabScroll.setOverScrollMode(View.OVER_SCROLL_NEVER); // disallow
																	// overscroll
		}

		// image dimensions and initialization
		final int dps = 175;
		final float scale = getApplicationContext().getResources()
				.getDisplayMetrics().density;
		pixels = (int) (dps * scale + 0.5f);
		leftPad = (int) (17 * scale + 0.5f);
		rightPad = (int) (15 * scale + 0.5f);
		height32 = (int) (32 * scale + 0.5f);
		tenPad = (int) (10 * scale + 0.5f);

		webpageOther = getResources().getDrawable(R.drawable.webpage);
		incognitoPage = getResources().getDrawable(R.drawable.incognito);
		webpageOther.setBounds(0, 0, width / 2, height / 2);
		incognitoPage.setBounds(0, 0, width / 2, height / 2);
		exitTab.setBounds(0, 0, width * 2 / 3, height * 2 / 3);

		Thread startup = new Thread(new Runnable() {

			@Override
			public void run() {
				// restores old tabs or creates a new one
			}

		});
		startup.run();
		reopenOldTabs();
		// new tab button
		ImageView newTab = (ImageView) findViewById(R.id.newTab);
		newTab.setBackgroundResource(R.drawable.button);
		newTab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Handler click = new NewTabHandler();
				click.sendEmptyMessage(1);
				tabScroll.postDelayed(new Runnable() {
					@Override
					public void run() {
						tabScroll.smoothScrollTo(currentTabTitle.getLeft(), 0);
					}
				}, 100L);

			}
		});
		newTab.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				if (settings.getString("oldPage", "").length() > 0) {
					newTab(settings.getString("oldPage", ""), true);
					edit.putString("oldPage", "");
					edit.commit();
					tabScroll.postDelayed(new Runnable() {
						@Override
						public void run() {
							tabScroll.smoothScrollTo(currentTabTitle.getLeft(),
									0);
						}
					}, 100L);
				}
				return true;
			}

		});
		refresh = (ImageView) findViewById(R.id.refresh);
		refreshLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (currentTab.getProgress() < 100) {
					currentTab.stopLoading();
				} else {
					currentTab.reload();
				}
			}

		});

		enterUrl();
		if (showFullScreen) {
			//@Mohu Evan Boucher: Commented this out so it is not called.
			//toggleFullScreen();
		}
		browserHandler = new Handle();
		
		//@Mohu Evan Boucher: MohuOne add the JavaScript interface for communicating.
		currentTab.getSettings().setJavaScriptEnabled(true);
		MohuOneHandler = new JavaScriptHandler(this);
		currentTab.addJavascriptInterface(MohuOneHandler, "MohuOneHandler");
		Log.d("MohuOne", "Initalize(): Added the JavaScript interface.");
		
		toggleFullScreen();
		//Hide the Url Bar at startup.
		//uBar.startAnimation(slideUp);
	}

	private void newSettings() {
		startActivity(new Intent(FinalVariables.SETTINGS_INTENT));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {
		if (requestCode == 1) {
			if (null == mUploadMessage)
				return;
			Uri result = intent == null || resultCode != RESULT_OK ? null
					: intent.getData();
			mUploadMessage.onReceiveValue(result);
			mUploadMessage = null;

		}
	}
	
	/*
	@Override
	public void onBackPressed() {
		try {
			
			 * @Mohu Evan Boucher: Continue with this. I have implemented the EnabledVideo Chrome Client
			 * but it still does not work. Figure out how to escape fullscreen video when the back 
			 * button is pressed. Going back in the browser will crash the browser. This 
			 * is most likely due to some issue which I cannot quite put my finger on. 
			 * Look into and make sure that the appropriate custom client is being created.
			 * NOTE: even so it may not be a solution as the video is embedded in the browser
			 * and is handled natively by the browser, AKA not within our control.
			 
			Log.d("MohuChannels", "onBack was pressed in Browser Activity.");
			Log.d("MohuChannels", "CurrentTab: " + currentTab.toString());
			//Log.d("MohuChannels", "currentTab.onBackPressed::> " + currentTab.onBackPressed());
			//@Mohu Evan Boucher: Added to handle the back button being pressed.
			
			if (currentTab.onBackPressed()) {
				Log.d("MohuChannels", "onBack was HANDLED BY WEb View!");
				//The web view handled the press so it is not needed here (video was being displayed).
				return;
			}
			
			if (showFullScreen && !uBar.isShown()) {
				Log.d("MohuChannels", "Back Pressed: Fullscreen is on and URL is not shown.");
				//@Mohu Evan Boucher: Changed so that no animation happens when you hit back. 
				//uBar.startAnimation(slideDown);
			}
			if (currentTab.isShown() && currentTab.canGoBack()) {
				Log.d("MohuChannels", "The back button was hit and the browser will attempt to go back.");
				currentTab.goBack();
			} else {
				Log.d("MohuChannels", "Fell through if statements and would normally delete tab (functionality removed).");
				//deleteTab(currentId);
				//@Mohu Evan Boucher: Ubar should not come to front.
				//uBar.bringToFront();
			}
		} catch (NullPointerException ignored) {
			Log.d("MohuChannels", "onBackPressed had Null Pointer. Exception Ignored.");
		}
		return;
	}
	*/
	
	@Override
	public void onBackPressed() {
		try {
			/*
			 * @Mohu Evan Boucher: Removed so the url bar does not come down when going back. Only comes down on alt+space
			if (showFullScreen && !uBar.isShown()) {
				uBar.startAnimation(slideDown);
			}
			*/
			if (BrowserActivity.canGoBack()) { // Use to be currentTab.canGoBack()
				if(currentTab.isShown()){
					currentTab.goBack();
				} else {
					//@Mohu Evan Boucher: Removed so that the browser does not hide its view on back. Instead, just toast.
					currentTab.onHideCustomView();
					/* Remove for now since we want to close the web view if we cannot go back.
					WebBackForwardList tab_history = currentTab.copyBackForwardList();
					Log.d("MohuChannels", "The length of the current history in onBackPressed()::> " + tab_history.getSize());
					Toast.makeText(CONTEXT, cannotGoBackMessage, Toast.LENGTH_LONG).show();;
					*/
				}
			} else {
				//@Mohu Evan Boucher: Commented out so that the user does not delete a tab when you hit back. instead toast.
				deleteTab(currentId);
				uBar.bringToFront();
				currentTab.onHideCustomView();
				/*
				WebBackForwardList tab_history = currentTab.copyBackForwardList();
				Log.d("MohuChannels", "The length of the current history in onBackPressed(canGoBack returned false)::> " + tab_history.getSize());
				Toast.makeText(CONTEXT, cannotGoBackMessage, Toast.LENGTH_LONG).show();
				*/
			}
		} catch (NullPointerException ignored) {
		}
		return;
	}
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (settings.getBoolean("textreflow", false)) {
			currentTab.getSettings().setLayoutAlgorithm(
					LayoutAlgorithm.NARROW_COLUMNS);
		} else {
			currentTab.getSettings().setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		setContentView(R.layout.activity_main); // displays main xml layout
		CONTEXT = this;
		ACTIVITY = this;
		settings = getSharedPreferences("settings", 0);
		edit = settings.edit();
		//if (settings.getBoolean("hidestatus", true)) { //@Mohu Evan Boucher: Commented out IF statment to alwasy hide status bar.
			getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//}
		if (settings.getBoolean("savetabs", true)) {
			String mem = settings.getString("memory", "");
			edit.putString("memory", "");
			memoryURL = new String[MAX_TABS];
			memoryURL = getArray(mem);
		}

		try {
			LocationManager locationManager = (LocationManager) CONTEXT
					.getSystemService(Context.LOCATION_SERVICE);
			if (locationManager.getAllProviders().contains(
					LocationManager.GPS_PROVIDER)) {
				DEVICE_HAS_GPS = true;
			}
		} catch (Exception ignored) {
			DEVICE_HAS_GPS = false;
		}
		inactive = getResources().getDrawable(R.drawable.bg_inactive);
		active = getResources().getDrawable(R.drawable.bg_press);
		initialize(); // sets up random stuff
		//@Mohu Evan Boucher: Removed so that options menu is not called or created. 
		options(); // allows options to be opened
		//@Mohu Evan Boucher: Added to make sure settings are created and is used.
		newSettings();
		
		enter();// enter url bar
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		float widthInInches = metrics.widthPixels / metrics.xdpi;
		float heightInInches = metrics.heightPixels / metrics.ydpi;
		double sizeInInches = Math.sqrt(Math.pow(widthInInches, 2)
				+ Math.pow(heightInInches, 2));
		// 0.5" buffer for 7" devices
		isPhone = sizeInInches < 6.5;
		forward();// forward button
		back();
		PackageInfo p;
		int code = 0;
		try {
			p = getPackageManager().getPackageInfo(getPackageName(), 0);
			code = p.versionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//@Mohu Evan Boucher: Add these channels as bookmarks so they cannot be added again. Preloaded web pages.
		//It is important for the URLs to end with a '/' in this. Even if the MohuApp adds them wihtout the '/'. (Proper URL).
		Utils.addBookmark(CONTEXT,"amazon", "http://www.amazon.com/");
		Utils.addBookmark(CONTEXT,"pandora", "http://www.pandora.com/");
		Utils.addBookmark(CONTEXT,"fox", "http://www.fox.com/");
		Utils.addBookmark(CONTEXT,"nbc", "http://www.nbc.com/");
		Utils.addBookmark(CONTEXT,"Google", "https://www.google.com/");
		Utils.addBookmark(CONTEXT,"MohuOne", "http://www.mohuone.com/"); 
		Utils.addBookmark(CONTEXT,"MohuOne", "http://www.mohuone.com/guide/"); 
		
		/*
		 * @Mohu Evan Boucher: Removed this so that it does not show up anymore. Leaving here for now.
		 */
		/*
		if (settings.getInt("first", 0) == 0) {
			// navigation tips
			String message = "1. Long-press back button to exit browser\n\n"
					+ "2. Swipe from left edge toward the right (---->) to go back\n\n"
					+ "3. Swipe from right edge toward the left (<----)to go forward\n\n"
					+ "4. Visit settings and advanced settings to change options\n\n"
					+ "5. Long-press on the new tab button to open the last closed tab";

			Utils.createInformativeDialog(CONTEXT, "Browser Tips", message);
			edit.putInt("first", 1);
			edit.putInt("version", code);
			edit.commit();
		} else if (settings.getInt("version", code - 1) != code) {
			edit.putInt("version", code);
			edit.commit();
		}
		*/
		//@Mohu Evan Boucher: FullScreen URLBar: Slide the bar up at the creation of the activity. Uncomment below to hide URL bar at start.
		//uBar.startAnimation(slideUp);
		//@Mohu Evan Boucher: Added to try and solve database issue when in full screen.
		String databasePath = currentTab.getContext().getDir("databases", 
                Context.MODE_PRIVATE).getPath(); 
currentTab.getSettings().setDatabasePath(databasePath);
		
		
		
		//@Mohu Evan Boucher: Create onclick listener for Fav Icon ImageButton.
		favicon.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				addBookmarkAsChannel(currentTab.getUrl());
			}
		});
	}
	
	public boolean checkIfBookmarkExists(String url) {
		return Utils.checkIfBookmarkExists(CONTEXT, url);
	}
	
	/**
	 * @Mohu Evan Boucher: Added to create a bookmark and add it as a channel.
	 */
	public boolean addBookmarkAsChannel(String url) {
		//@Mohu Evan Boucher: Added to check if the page connected to the server or not.
		if (!currentTab.isOnline(CONTEXT)) {
			//No internet access, so do not add this page to the bookmarks.
			Toast.makeText(CONTEXT, "No internet connection. Channel will not be created. Check your network settings.", Toast.LENGTH_LONG).show();
			return false;
		}
		
		//This is the actual page title.
		String pageTitle = currentTab.getTitle();
		Log.d("MohuChannels", "The currentTab URL is::> " + url);
		if (url == null) {
			//@Mohu Evan Boucher: The url is null so we cannot do anything. Invalid page.
			Toast.makeText(CONTEXT, "Invalid webpage. Please reload the page or check that the URL is correct.", Toast.LENGTH_LONG).show();
			return false;
		}
		
		//Check if pageTitle was not defined.
		try {
			//if (pageTitle == null || pageTitle.equals("")) {
				Log.d("MohuChannels", "The current webpage: " + url + " pageTitle was empty.");
				Pattern urlRegex = Pattern.compile("http[s]*://w*.*[a-z0-9]+:*[0-9]*.[a-z]*"); 
				//Include non standard: http[s]*://w*.*[a-z0-9]+:*[0-9]*.[a-z]*
				//Return only the gut of a URL ({www.}<address>.<extension>): http[s]*://w*.*[a-z]+.[a-z]+
				Matcher m = urlRegex.matcher(url);
				if (m.find()){
					if (pageTitle == null || pageTitle.equals("")) {
						pageTitle = m.group(0);
						Log.d("MohuChannels", "The regex returned this::> " + pageTitle);
					}
				} else {
					//The regex did not find a proper http page, so do not add as bookmark.
					//@Mohu Evan Boucher: 
					Log.d("MohuChannels", "Regex did not return a valid URL:> " + url);
					Toast.makeText(CONTEXT, "Bad URL...No Channel can be created.", Toast.LENGTH_LONG).show();
					return false;
				}
				
			//}
		} catch (Exception e) {
			//The regex did not find a proper http page, so do not add as bookmark.
			//@Mohu Evan Boucher: 
			Log.d("MohuChannels", "Exception while parsing the regex...Current URL:> " + url);
			Toast.makeText(CONTEXT, "Bad URL...No Channel can be created.", Toast.LENGTH_LONG).show();
			return false;
		}
		/*
		} catch (Exception e) {
			//A null pointer happened, so catch it and replace pageTitle with Unknown.
			Log.d("MohuChannels", "The current webpage: " + url + " pageTitle was empty.");
			//Set pageTitle to url, just incase the url is invalid for the regex.
			pageTitle = url;
			Pattern urlRegex = Pattern.compile("http[s]*://w+.[a-z]+.[a-z]+");
			Matcher m = urlRegex.matcher(url);
			if (m.find()){
				pageTitle = m.group(0);
				Log.d("MohuChannels", "The regex returned this::> " + pageTitle);
			} else {
				//The regex did not find a proper http page, so do not add as bookmark.
				//@Mohu Evan Boucher: 
				Log.d("MohuChannels", "Regex did not return a valid URL:> " + url);
				Toast.makeText(CONTEXT, "Bad URL...No Channel can be created.", Toast.LENGTH_LONG).show();
				return;
			}
		}
		*/
		Log.d("MohuChannels", "The title of the current page is::> " + pageTitle);
		if (url != null) {
			//If the page is the bookmarks page, then do not continue (this shouldn't happen, but a precaution).
			if (!urlToLoad[currentId][1].equals("Bookmarks")) {
				boolean channel_exists = Utils.addBookmark(CONTEXT, pageTitle, url); //Changed this to pass in pageTitle instead of url (middle param)
				if (channel_exists) {
					//Change the icon to the "Pressed Icon"
					//currentTab.faviconOn = true;
					favicon.setSelected(true);
					
					/*
					 * Create an intent to add the bookmark as a channel.
					 */
					Log.d("MohuChannels", "Lightning Browser is creating the intent for MohuApp to recieve.");
					Intent addChannelIntent = new Intent("acr.browser.barebones.activities.addWebChannelFromBookmark");
					addChannelIntent.putExtra("pageTitle", pageTitle);
					addChannelIntent.putExtra("webURL", url);
					//addChannelIntent.putExtra("shortURL", short_url);
					Log.d("MohuChannels", "Lightning Browser is broadcasting the intent!");
					CONTEXT.sendBroadcast(addChannelIntent);
					
					Toast.makeText(CONTEXT, "Added " + url + " as a Channel!", Toast.LENGTH_LONG).show();
					Log.d("MohuChannels", "About to turn off the Favicon since it was clicked.");
					favicon.setSelected(false);
					return true;
			} else {
				Toast.makeText(CONTEXT, "The Channel " + url + " already exists. Check your Channel Guide.", Toast.LENGTH_LONG).show();
				return false;
			}
		}
	} 
	//@Mohu Evan Boucher: 
	Toast.makeText(CONTEXT, "Bad URL...No Channel can be created.", Toast.LENGTH_LONG).show();
	return false;
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);

		return true;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onKeyDown(int, android.view.KeyEvent)
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d("MohuChannels", "Recieved key evet for onKeyDOWN() function::> " + keyCode);
		switch (keyCode) {
		case KeyEvent.KEYCODE_SEARCH: {
			getUrl.requestFocus();
			InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			manager.showSoftInput(getUrl, 0);
			break;
		}
		case KeyEvent.KEYCODE_F5: {
			currentTab.reload();
			break;
			//return true;
		}
		case KeyEvent.KEYCODE_ESCAPE: {
			currentTab.stopLoading();
			Log.d("MohuChannels", "Escape button was hit!!!!");
			break;
		}
		case KeyEvent.KEYCODE_TAB: {
			InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!manager.isActive()) {   
				//newTab(homepage, true);
				//@Mohu Evan Boucher: Commented out above so a newtab is not created.
				currentTab.loadUrl(homepage);
			}
			break;
		}
		case KeyEvent.KEYCODE_F12: {
			clearCache();
			finish();
			break;
		}
		case KeyEvent.KEYCODE_F6: {
			getUrl.selectAll();
			break;
		}
		case KeyEvent.KEYCODE_F10: {
			/*
			 * @Mohu Evan Boucher: Debugging to see if this is causing the settings to open on crash/Back.
			 * We never want the settings to open. Commented this out.
			 */
			//Utils.showToast(CONTEXT, "You just hit F10 which opened Settings.");
			//startActivity(new Intent(FinalVariables.SETTINGS_INTENT));
			break;
		}
		case KeyEvent.KEYCODE_F11: {
			//toggleFullScreen();
			break;
		}
		case KeyEvent.KEYCODE_DEL: {
			InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!manager.isActive()) {
				currentTab.goBack();
			}
			//return true;
			break;
		}
		//@Mohu Evan Boucher: KeyCode 4 is the back button that is pressed in Android.
		
		case KeyEvent.KEYCODE_BACK: {
			Log.d("MohuChannels", "Clicked the hard back button.");
			
			InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			if (!manager.isActive()) {
				currentTab.goBack();
			}
			
			break;
		}
		
		}
		return super.onKeyDown(keyCode, event);
	}
	
	/*
	 * Scroll the window up or down.
	 */
	public static boolean scrollUp() {
		if (currentTab.canScrollVertically(-80)) {
			currentTab.scrollBy(0, -80);
		}
		return true;
	}
	
	/*
	 * Scroll the window down
	 * @Mohu Evan Boucher 
	 */
	public static boolean scrollDown() {
		
		if (currentTab.canScrollVertically(80)) {
			currentTab.scrollBy(0, 80);
		}
		return true;
	}
	
	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (!settings.getBoolean("restoreclosed", true)) {
				for (int n = 0; n < MAX_TABS; n++) {
					urlToLoad[n][0] = null;
				}
			}
			//@Mohu Evan Boucher: Removed so that the activity does not finish itself. Just pass this up.
			clearCache();
			finish();
			//return true;
			currentTab.onHideCustomView();
			/*
			WebBackForwardList tab_history = currentTab.copyBackForwardList();
			Log.d("MohuChannels", "The length of the current history in onKeyLongPress method::> " + tab_history.getSize());
			Toast.makeText(CONTEXT, cannotGoBackMessage, Toast.LENGTH_LONG).show();
			*/
			return super.onKeyLongPress(keyCode, event);
		} else
			return super.onKeyLongPress(keyCode, event);
	}
	
	@Override
	public void onLowMemory() {
		for (int n = 0; n < MAX_TABS; n++) {
			if (n != currentId && main[n] != null) {
				main[n].freeMemory();
			}
		}
		super.onLowMemory();
	}

	@Override
	protected void onNewIntent(Intent intent) {

		String url = intent.getDataString();
		int id = -1;
		int download = -1;
		try {
			id = intent.getExtras().getInt("acr.browser.barebones.Origin") - 1;
		} catch (NullPointerException e) {
			id = -1;
		}
		try {
			download = intent.getExtras().getInt(
					"acr.browser.barebones.Download");
		} catch (NullPointerException e) {
			download = -1;
		}
		if (id >= 0) {
			main[id].loadUrl(url);
		} else if (download == 1) {
			Utils.downloadFile(CONTEXT, url, null, null);
		} else if (url != null) {
			//@Mohu Evan: removed this so we can replace the current tab URL instead of making new one.
			//newTab(url, true);
			currentTab.loadUrl(url);
		}

		super.onNewIntent(intent);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.history:
			generateHistory(currentTab, CONTEXT);
			return true;
		case R.id.bookmark:
			if (urlToLoad[currentId][1] != null) {
				if (!urlToLoad[currentId][1].equals("Bookmarks")) {
					Utils.addBookmark(CONTEXT, urlToLoad[currentId][1],
							urlToLoad[currentId][0]);
				}
			}
			return true;
			/*
			 * @Mohu Evan Boucher: Remove access to settings from the drop down menu.
		case R.id.settings:
			newSettings();
			return true;
			*/
			/*
			 * @Mohu Evan Boucher: Removed so that bookmarks could not be accessed. 
		case R.id.allBookmarks:
			if (urlToLoad[currentId][1] == null) {
				goBookmarks(CONTEXT, currentTab);
			} else if (!urlToLoad[currentId][1].equals("Bookmarks")) {
				goBookmarks(CONTEXT, currentTab);
			}

			return true;
			*/
			/*
			 * @Mohu Evan Boucher: Removed because the device does not have bluetooth to use share.
		case R.id.share:
			share();
			return true;
			*/
			/*
			 * @Mohu Evan Boucher: Removed so no new tab can be created.
		case R.id.incognito:
			startActivity(new Intent(FinalVariables.INCOGNITO_INTENT));
			// newTab(number, homepage, true, true);
			return true;
			*/
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onPause() {
		if (currentTab != null) {
			if (API >= 11) {
				currentTab.onPause();
				for (int n = 0; n < MAX_TABS; n++) {
					if (main[n] != null)
						main[n].onPause();
				}
			}
			currentTab.pauseTimers();
		}

		Thread remember = new Thread(new Runnable() {

			@Override
			public void run() {
				String s = "";
				for (int n = 0; n < MAX_TABS; n++) {
					if (urlToLoad[n][0] != null) {
						s = s + urlToLoad[n][0] + "|$|SEPARATOR|$|";
					}
				}
				edit.putString("memory", s);
				edit.commit();
			}
		});
		remember.start();
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (currentTab != null) {
			onProgressChanged(currentId, currentTab.getProgress());
			if (currentTab.getProgress() == 100) {
				progressBar.setVisibility(View.GONE);
				refresh.setVisibility(View.VISIBLE);

			}
			if (API >= 11) {
				currentTab.onResume();
				for (int n = 0; n < MAX_TABS; n++) {
					if (main[n] != null)
						main[n].onResume();
				}
			}
		}
		gestures = settings.getBoolean("gestures", true);
		currentTab.resumeTimers();
		reinitializeSettings();
		if (settings.getBoolean("fullscreen", false) != fullScreen) {
			//@Mohu Evan Boucher: FullScreen URLBar Issue: Removed to ensure it is only called once.
			//toggleFullScreen();
		}

	}

	private int x;
	private int y;
	private boolean xPress;
	private Rect edge;
	private final GestureDetector mGestureDetector = new GestureDetector(
			CONTEXT, new CustomGestureListener());

	private class CustomGestureListener extends SimpleOnGestureListener {
		@Override
		public void onLongPress(MotionEvent e) {
			deleteTab(id);
			super.onLongPress(e);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		id = v.getId();
		try {
			background.clearDisappearingChildren();
			xPress = false;
			x = (int) event.getX();
			y = (int) event.getY();
			edge = new Rect();
			v.getDrawingRect(edge);
			currentTabTitle.setPadding(leftPad, 0, rightPad, 0);
			if (event.getAction() == MotionEvent.ACTION_DOWN) {
			} else if (event.getAction() == MotionEvent.ACTION_UP) {

				if (x >= (edge.right - bounds.width() - v.getPaddingRight() - 10 * 3 / 2)
						&& x <= (edge.right - v.getPaddingRight() + 10 * 3 / 2)
						&& y >= (v.getPaddingTop() - 10 / 2)
						&& y <= (v.getHeight() - v.getPaddingBottom() + 10 / 2)) {
					xPress = true;
				}
				if (id == currentId) {
					if (xPress) {
						deleteTab(id);
						uBar.bringToFront();
					}
				} else if (id != currentId) {
					if (xPress) {
						deleteTab(id);
					} else {
						if (API < 16) {
							currentTabTitle.setBackgroundDrawable(inactive);
						} else if (API > 15) {
							currentTabTitle.setBackground(inactive);
						}
						currentTabTitle.setPadding(leftPad, 0, rightPad, 0);
						if (!showFullScreen) {
							background.addView(main[id]);
							main[id].startAnimation(fadeIn);
							currentTab.startAnimation(fadeOut);
							background.removeView(currentTab);
							uBar.bringToFront();
						} else if (API >= 12) {
							main[id].setAlpha(0f);
							background.addView(main[id]);
							try {
								main[id].animate().alpha(1f).setDuration(250);
							} catch (NullPointerException ignored) {
							}
							background.removeView(currentTab);
							uBar.bringToFront();
						} else {
							background.removeView(currentTab);
							background.addView(main[id]);
						}
						uBar.bringToFront();
						
						currentId = id;
						currentTab = main[id];
						currentTabTitle = urlTitle[id];
						setUrlText(urlToLoad[currentId][0]);
						getUrl.setPadding(tenPad, 0, tenPad, 0);
						
						//@Mohu Evan Boucher: Added to find where onclicktab is.
						Log.d("MohuChannels", "You clicked a new tab!, called onTouch function.");
						if (currentTab.faviconOn) {
							favicon.setSelected(true);
						} else {
							favicon.setSelected(false);
						}
						if (API < 16) {
							currentTabTitle.setBackgroundDrawable(active);
						} else if (API > 15) {
							currentTabTitle.setBackground(active);
						}
						if (currentTab.getProgress() < 100) {
							refresh.setVisibility(View.INVISIBLE);

							progressBar.setVisibility(View.VISIBLE);

						} else {
							progressBar.setVisibility(View.GONE);
							refresh.setVisibility(View.VISIBLE);
						}
						onProgressChanged(currentId, currentTab.getProgress());
						tabScroll.smoothScrollTo(currentTabTitle.getLeft(), 0);
						currentTab.invalidate();
					}
				}

			}
			uBar.bringToFront();
			v.setPadding(leftPad, 0, rightPad, 0);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Lightning Error", "Well we dun messed up");
		}
		mGestureDetector.onTouchEvent(event);
		return true;
	}
	
	/*
	 * @Mohu Evan Boucher: Added to check if the currentTab page is online or not.
	 */																	
	public boolean isOnline() {
	    ConnectivityManager cm =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	
	    return cm.getActiveNetworkInfo() != null && 
	       cm.getActiveNetworkInfo().isConnectedOrConnecting();
	}

	
	/*
	 * @Mohu Evan Boucher: Removed so that the user cannot access the options menu in browser.
	 */
	void options() {
		ImageView options = (ImageView) findViewById(R.id.options);
		options.setBackgroundResource(R.drawable.button);
		options.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (API >= 11) {
					PopupMenu menu = new PopupMenu(CONTEXT, v);
					MenuInflater inflate = menu.getMenuInflater();
					inflate.inflate(R.menu.menu, menu.getMenu());
					menu.setOnMenuItemClickListener(new OnMenuItemClickListener() {

						@Override
						public boolean onMenuItemClick(MenuItem item) {

							switch (item.getItemId()) {
							case R.id.history:
								generateHistory(currentTab, CONTEXT);
								return true;
							case R.id.bookmark:
								addBookmarkAsChannel(currentTab.getUrl());
								return true;
								/*
								 * @Mohu Evan Boucher: Removed the settings item from the pop up menu
							case R.id.settings:
								newSettings();
								return true;
								*/
								/*
								 * @Mohu Evan Boucher: Removed so the user cannot access the bookmarks.
							case R.id.allBookmarks:
								if (urlToLoad[currentId][1] == null) {
									goBookmarks(CONTEXT, currentTab);
								} else if (!urlToLoad[currentId][1]
										.equals("Bookmarks")) {
									goBookmarks(CONTEXT, currentTab);
								}
								return true;
								*/
								/*
								 * @Mohu Evan Boucher: Removed since device does not have bluetooth. 
							case R.id.share:
								share();
								return true;
								*/
								/*
								 * @Mohu Evan Boucher: Removed since incognito is no an option.
							case R.id.incognito:
								startActivity(new Intent(
										FinalVariables.INCOGNITO_INTENT));
								// newTab(number, homepage, true, true);
								return true;
								*/
							case R.id.about:
								//Code for starting the about page.
								Log.d("MohuChannels", "You clicked the about icon.");
								startActivity(new Intent(LICENSE_INTENT));
								return true;
							default:
								return false;
							}

						}

					});
					menu.show();
				} else if (API < 11) {

					openOptionsMenu();
				}
			}

		});
	}
	

	void reopenOldTabs() {
		Intent url = getIntent();
		String URL = url.getDataString();
		boolean oldTabs = false;

		if (settings.getBoolean("savetabs", true)) {
			if (URL != null) {
				// opens a new tab with the url if its there
				int n = newTab(URL, true);
				main[n].resumeTimers();
				oldTabs = true;

			}
			boolean first = false;
			for (String aMemoryURL : memoryURL) {
				if (aMemoryURL.length() > 0) {
					if (!first) {
						int n = newTab("", !oldTabs);
						main[n].resumeTimers();
						main[n].getSettings().setCacheMode(
								WebSettings.LOAD_CACHE_ELSE_NETWORK);
						main[n].loadUrl(aMemoryURL);
					} else {
						int n = newTab("", false);
						main[n].getSettings().setCacheMode(
								WebSettings.LOAD_CACHE_ELSE_NETWORK);
						main[n].loadUrl(aMemoryURL);
					}
					oldTabs = true;
				}

			}

			if (!oldTabs) {
				int n = newTab(homepage, true);
				main[n].resumeTimers();
			}
		} else {
			if (URL != null) {
				// opens a new tab with the URL if its there
				//int n = newTab(URL, true);
				//@Mohu Evan Boucher: Commented out above so a newtab is not created.
				currentTab.loadUrl(URL);

				main[0].resumeTimers();

			} else {
				// otherwise it opens the home-page
				//int n = newTab(homepage, true);
				currentTab.loadUrl(homepage);
				main[0].resumeTimers(); //@Mohu Evan Boucher: Changed this to 0. Use to be 'n' 

			}
		}
	}
	
	//**************History methods*********************//
	////@Mohu Evan Boucher: Changed to call our custom history loadUrl. SEARCH THIS TO REPLACE BACK TO currentTab.loadUrl(
	/**
	 * This will add the given url to the custom history we implemented. It also lists the current history in the debug log.
	 * @param url
	 */
	public static void addToHistory(String url) {
		historyList.addHistory(url);
		Log.d("MohuHistory", "At the end of addToHistory() in Browser Activity. about to list all saved URLs.");
		historyList.listHistory();
	}
	/**
	 * This will go forward in our history list if possible. The current index is updated.
	 * @return The previous history object.
	 */
	public static HistoryObject goForwardHistory() {
		HistoryObject temp = historyList.goForward_Custom();
		return temp;
	}
	/**
	 * This will go back in our history list if possible. The current index is updated.
	 * @return The next history item in the list.
	 */
	public static HistoryObject goBackHistory() {
		historyList.listHistory();
		return historyList.goBack_Custom();
	}
	
	/**
	 * Returns if the browser can go back or not.
	 * @return
	 */
	public static boolean canGoBack() {
		if (historyList.prevIndex() < 0) {
			Log.d("MohuChannels", "Returned false from canGoBack().");
			return false;
		}
		return true;
	}
	
	
	//@Mohu Evan Boucher test the size of the window currently.
	public Point getWindowSize() {
		Point size = new Point();
		getWindowManager().getDefaultDisplay().getSize(size);
		return size;
	}
	
	
}