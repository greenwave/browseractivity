package acr.browser.barebones.webviewclasses;

import acr.browser.barebones.R;
import acr.browser.barebones.activities.BrowserActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebStorage.QuotaUpdater;
import android.widget.FrameLayout;
import android.widget.VideoView;

@SuppressLint("SetJavaScriptEnabled")
public class CustomChromeClient extends WebChromeClient {
	private static Context context;
	private static Activity browserActivity;
	private static View mCustomView;
	private static CustomViewCallback mCustomViewCallback;
	public CustomChromeClient(BrowserActivity activity){
		context = activity;
		browserActivity = activity;
	}
	public Bitmap mDefaultVideoPoster;
	public View mVideoProgressView;
	public FrameLayout fullScreenContainer;
	public int orientation;

	@Override
	public void onExceededDatabaseQuota(String url,
			String databaseIdentifier, long quota,
			long estimatedDatabaseSize, long totalQuota,
			QuotaUpdater quotaUpdater) {
		quotaUpdater.updateQuota(totalQuota + estimatedDatabaseSize);

	}

	@Override
	public void onProgressChanged(WebView view, int newProgress) {
		BrowserActivity.onProgressChanged(view.getId(), newProgress);
		super.onProgressChanged(view, newProgress);
	}

	@Override
	public void onReachedMaxAppCacheSize(long requiredStorage, long quota,
			QuotaUpdater quotaUpdater) {
		quotaUpdater.updateQuota(quota + requiredStorage);
	}

	@Override
	public Bitmap getDefaultVideoPoster() {
		if (mDefaultVideoPoster == null) {
			mDefaultVideoPoster = BitmapFactory.decodeResource(
					context.getResources(), android.R.color.black);
		}
		return mDefaultVideoPoster;
	}

	@Override
	public View getVideoLoadingProgressView() {
		if (mVideoProgressView == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			mVideoProgressView = inflater.inflate(
					android.R.layout.simple_spinner_item, null);
		}
		return mVideoProgressView;
	}

	@Override
	public void onCloseWindow(WebView window) {
		Message msg = Message.obtain();
		msg.what = 3;
		msg.arg1 = window.getId();
		BrowserActivity.browserHandler.sendMessage(msg);
		super.onCloseWindow(window);
	}

	@Override
	public boolean onCreateWindow(WebView view, boolean isDialog,
			boolean isUserGesture, final Message resultMsg) {
		//return false;
		if (isUserGesture) {
			Log.d("MohuChannels", "The view URL within onCreateWindow for CustomChromeClient...::> " + view.getUrl());
			Log.d("MohuChannels", "The description of the message in onCreateWindow within CustomChromeClient::> " + resultMsg.toString());
			BrowserActivity.onCreateWindow(resultMsg);
		}
		return true;
		
	}
	
	
	@Override
	public void onGeolocationPermissionsShowPrompt(final String origin,
			final GeolocationPermissions.Callback callback) {
			final boolean remember = true;
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle("Location Access");
			String org = null;
			if (origin.length() > 50) {
				org = (String) origin.subSequence(0, 50) + "...";
			} else {
				org = origin;
			}
			builder.setMessage(org + "\nWould like to use your Location ")
					.setCancelable(true)
					.setPositiveButton("Allow",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int id) {
									callback.invoke(origin, true, remember);
								}
							})
					.setNegativeButton("Don't Allow",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int id) {
									callback.invoke(origin, false, remember);
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		
	}

	@Override
	public void onHideCustomView() {
		if (mCustomView == null && mCustomViewCallback == null) {
			return;
		}
		/*
		 * @Mohu Evan Boucher: Added to catch null view since we cannot process it.
		 */
		if (mCustomView == null) {
			Log.d("MohuChannels", "The mCustomView was null inside the onHideCustomView() of CustomChromeClient.java");
			return;
		}
		mCustomView.setKeepScreenOn(false);
		mCustomView = null;
		Log.d("MohuChannels", "within hideCustomView() of CustomChromeClient.");
		BrowserActivity.onHideCustomView(fullScreenContainer, mCustomViewCallback, orientation);
		super.onHideCustomView();
	}

	@Override
	public void onReceivedIcon(WebView view, Bitmap favicon) {
		BrowserActivity.setFavicon(view.getId(), favicon);
	}

	@Override
	public void onReceivedTitle(final WebView view, final String title) {
		Log.d("MohuHistory", "onReceivedTitle() within ChromClient: Calling BrowserActiity.onReceivedTitle().");
		BrowserActivity.onReceivedTitle(view.getId(), title);
		super.onReceivedTitle(view, title);
	}

	@Override
	public void onShowCustomView(View view, int requestedOrientation,
			CustomViewCallback callback) {
		if (mCustomView != null) {
			callback.onCustomViewHidden();
			return;
		}
		view.setKeepScreenOn(true);
		orientation = browserActivity.getRequestedOrientation();
		FrameLayout screen = (FrameLayout) browserActivity.getWindow().getDecorView();
		fullScreenContainer = new FrameLayout(context);
		fullScreenContainer.setBackgroundColor(context.getResources().getColor(
				R.color.black));
		BrowserActivity.onShowCustomView();
		fullScreenContainer.addView(view,
				ViewGroup.LayoutParams.MATCH_PARENT);
		screen.addView(fullScreenContainer,
				ViewGroup.LayoutParams.MATCH_PARENT);
		mCustomView = view;
		mCustomViewCallback = callback;
		browserActivity.setRequestedOrientation(requestedOrientation);
		
		//@Mohu Evan Boucher: Added to try to get videos to play fullscreen by default.
		android.net.Uri mUri = null;

		try
		{
		    @SuppressWarnings("rawtypes")
		    Class _VideoSurfaceView_Class_ = Class.forName("android.webkit.HTML5VideoFullScreen$VideoSurfaceView");

		    java.lang.reflect.Field _HTML5VideoFullScreen_Field_ = _VideoSurfaceView_Class_.getDeclaredField("this$0");

		    _HTML5VideoFullScreen_Field_.setAccessible(true);

		    Object _HTML5VideoFullScreen_Instance_ = _HTML5VideoFullScreen_Field_.get(((FrameLayout) view).getFocusedChild());

		    @SuppressWarnings("rawtypes")
		    Class _HTML5VideoView_Class_ = _HTML5VideoFullScreen_Field_.getType().getSuperclass();

		    java.lang.reflect.Field _mUri_Field_ = _HTML5VideoView_Class_.getDeclaredField("mUri");

		    _mUri_Field_.setAccessible(true);

		    mUri =  (Uri) _mUri_Field_.get(_HTML5VideoFullScreen_Instance_);
		}
		catch (Exception ex)
		{   
			ex.printStackTrace();
			Log.d("MohuChannels", "Exception in onShowCustomView for CustomChromeClient. Video failed to close.");
		}
		if (mUri != null)
		{
		    // There you have, mUri is the URI of the video
			Log.d("MohuChannels", "the mUri is not null.");
		}

		
		super.onShowCustomView(view, requestedOrientation, callback);
	}
	
	

	@Override
	public void onShowCustomView(View view,
			WebChromeClient.CustomViewCallback callback) {
		if (mCustomView != null) {
			callback.onCustomViewHidden();
			return;
		}
		view.setKeepScreenOn(true);
		orientation = browserActivity.getRequestedOrientation();
		FrameLayout screen = (FrameLayout) browserActivity.getWindow().getDecorView();
		fullScreenContainer = new FrameLayout(context);
		fullScreenContainer.setBackgroundColor(context.getResources().getColor(
				R.color.black));
		BrowserActivity.onShowCustomView();
		fullScreenContainer.addView(view,
				ViewGroup.LayoutParams.MATCH_PARENT);
		screen.addView(fullScreenContainer,
				ViewGroup.LayoutParams.MATCH_PARENT);
		mCustomView = view;
		mCustomViewCallback = callback;
		browserActivity.setRequestedOrientation(browserActivity.getRequestedOrientation());
		super.onShowCustomView(view, callback);
	}

	public void openFileChooser(ValueCallback<Uri> uploadMsg) {
		BrowserActivity.openFileChooser(uploadMsg);
	}

	public void openFileChooser(ValueCallback<Uri> uploadMsg,
			String acceptType) {
		BrowserActivity.openFileChooser(uploadMsg);
	}

	public void openFileChooser(ValueCallback<Uri> uploadMsg,
			String acceptType, String capture) {
		BrowserActivity.openFileChooser(uploadMsg);
	}

}
