package acr.browser.barebones.utilities;

import java.util.LinkedList;
import java.util.ListIterator;

import acr.browser.barebones.utilities.CustomHistoryList.HistoryObject;
import android.util.Log;

public class CustomHistoryList {
	public class HistoryObject extends Object{
		public String url;
		
		public HistoryObject(String url) {
			this.url = url;
		}
		/**
		 * Custom equals method for comparing the two URLs to see if they are equal.
		 * @param history
		 * @return
		 */
		public boolean equals(HistoryObject history) {
			if (this.url.equalsIgnoreCase(history.url)) {
				return true;
			}
			return false;
		}
	}
	private LinkedList<HistoryObject> history = new LinkedList<HistoryObject>();
	private int cur_index = -1;
	private ListIterator<HistoryObject> iter;
	
	private boolean dirtyList = false;
	
	/**
	 * Returns if the list is dirty or not.
	 * @return
	 */
	public boolean isDirtyList() {
		return dirtyList;
	}
	
	/**
	 * This cleans the list by removing all entries including and after the current index. 
	 * This will set the dirtyList to false (making it a clean list).
	 */
	public void cleanList() {
		Log.d("MohuHistory", "List is about to be cleaned. Cur index:> " + cur_index);
		removeAllAfterIndex(cur_index);
		dirtyList = false;
		Log.d("MohuHistory", "List was cleaned. NEW index:> " + cur_index);
	}

	public CustomHistoryList(){
		iter = history.listIterator();
	}
	
	public LinkedList<HistoryObject> getHistory() {
		return history;
	}
	public int getIndex() {
		return cur_index;
	}
	public void setHistory_index(int history_index) {
		this.cur_index = history_index;
	}
	
	
	public HistoryObject goBack_Custom() {
		//Check if the current position has a previous.
		Log.d("MohuHistory", "goBack_Custom(): the Prev index is:> " + (cur_index - 1));
		if (hasPrev()) {
			Log.d("MohuHistory", "goBack_Custom(): Cur  (before setting to new index) index is:> " + getIndex());
			Log.d("MohuHistory", "goBack_Custom(): The next index is:> " + nextIndex());
			HistoryObject temp = getPrevIndex();
			Log.d("MohuHistory", "goBack_Custom(): temp prev was:> " + temp.url);
			Log.d("MohuHistory", "goBack_Custom(): The new index is:> " + cur_index);
			
			//The list is now dirty.
			dirtyList = true;
			return temp;
			
		} 
		Log.d("MohuHistory", "goBack_Custom(): returned null.");
		return null;
	}
	
	public HistoryObject goForward_Custom() {
		Log.d("MohuHistory", "goForward_Custom(): the next index is:>" + iter.nextIndex());
		if (hasNext()) {
			Log.d("MohuHistory", "goForward_Custom(): iter has a next. Cur index:> " + getIndex() + " \nThe next index is:> " + nextIndex());
			HistoryObject temp = getNextIndex();
			Log.d("MohuHistory", "goForward_Custom(): iters next item was:> " + temp.url);
			Log.d("MohuHistory", "goForward_Custom(): The new index is:> " + cur_index);

			//The list is now dirty 
			dirtyList = true;
			return temp;
		}
		Log.d("MohuHistory", "goForward_Custom(): returned null.");
		return null;
	}
	
	public void addHistory(String url) {
		Log.d("MohuHistory", "addHistory(): Adding the url:> " + url + " to the history list.");
		if (url == null || url.equalsIgnoreCase("") || url.equalsIgnoreCase("about:blank")) {
			Log.d("MohuHistory", "addHistory():> bad Url or page. Nothing added.");
			return;
		} 
		if (dirtyList) {
			Log.d("MohuHistory", "addHistory(): The list is dirty! Cleaning it at index:> " + cur_index);
			cleanList();
		}
		Log.d("MohuHistory", "addHistory(): The list hasPrev():> " + hasPrev());
		if (hasPrev() && history.get(cur_index).url.equalsIgnoreCase(url)) {
			Log.d("MohuHistory", "goBack_Custom(): DUPLICATE! The last two items where each so we should not add them.");
			return;
		}
		history.add(new HistoryObject(url));
		cur_index++; 
		
		//Set the new list iterator to the index of the previous one. Iterators have no current object. it lies between prev and next.
		Log.d("MohuHistory", "addHistory(): The current index is:> " + getIndex());
		Log.d("MohuHistory", "addHistory(): The next index:> " + nextIndex());

	}
	/**
	 * This removes all items starting at and after the given list. Chops off the tail at the given index. 
	 * Sets the current index to point to the last object of the new size list.
	 * @param index The index to cut off (including the index given).
	 */
	private void removeAllAfterIndex(final int index) {
		Log.d("MohuHistory", "removeAllAfterIndex(): The index passed in:> " + index);
		Log.d("MohuHistory", "removeAllAfterIndex(): The size of history before removal:> " + history.size());
		int cur_size = history.size()-1;
		for (int i = cur_size; i > index; i--) {
			Log.d("MohuHistory", "Url at " + i + ":> " + history.remove(i).url + " has been REMOVED!");
		}
		cur_index = history.size() - 1;
	}
	/**
	 * Returns a string with the current History list enumerated on new lines.
	 * @return
	 */
	public String listHistory() {
		Log.d("MohuHistory", "About to enurmerate the history list...Cur index:> " + cur_index);
		String list = "";
		ListIterator<HistoryObject> tempiter = history.listIterator();
		HistoryObject temp;
		int i = 0;
		while(tempiter.hasNext()) {
			temp = tempiter.next();
			Log.d("MohuHistory", "url " + i + " :> " + temp.url);
			list = list + (temp.url + "\n");
			i++;
		}
		return list;
	}
	
	/**
	 * Returns the next index in the list. Does NOT set the new index.
	 * @return
	 */
	public int prevIndex() {
		return cur_index - 1;
	}
	/**
	 * Returns the next index. Does NOT set the index to the next object.
	 * @return
	 */
	public int nextIndex() {
		return cur_index + 1;
	}
	/**
	 * Returns the next HistoryObject and sets the current index to that object.
	 * @return
	 */
	public HistoryObject getNextIndex() {
		++cur_index;
		return history.get(cur_index);
	}
	/**
	 * Returns the previous HistoryObject and sets the current index to that object.
	 * @return
	 */
	public HistoryObject getPrevIndex() {
		--cur_index;
		return history.get(cur_index);
	}
	/**
	 * Custom method for determining if there is a previous index.
	 * @return
	 */
	public boolean hasPrev() {
		if (cur_index > 0) {
			Log.d("MohuHistory", "hasPrev(): The list has a previous item. cur index:> " + cur_index);
			return true;
		}
		return false;
	}
	
	/**
	 * Custom method for determining if there is a next() index.
	 * @return
	 */
	public boolean hasNext() {
		if (cur_index < (history.size() -1)) {
			return true;
		}
		return false;
	}
	/**
	 * Returns the current history object, no changes made to list or index.
	 * @return The current history object we are at.
	 */
	public HistoryObject getCurIndex() {
		if (cur_index < 0) {
			return new HistoryObject("");
		}
		return history.get(cur_index);
	}
	
	/**
	 * Replaces the current historyObject URL with the given url.
	 * @param url The url to replace the currentIndex
	 * @return Returns the updated HistoryObject
	 */
	public HistoryObject replaceCurIndex(final String url) {
		if (cur_index <= 0) {
			return null;
		}
		try {
			history.get(cur_index).url = url;
			Log.d("MohuGoogle", "replaceCurIndex(): The cur_index is:> " + cur_index);
			Log.d("MohuGoogle", "replaceCurIndex(): The objects new url::> " + history.get(cur_index).url);
			return history.get(cur_index);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			Log.d("MohuHistory", "The index went out of bounds. Should not be an issue.");
		}
		return null;
	}
	
}
