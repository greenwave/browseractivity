package acr.browser.barebones.utilities;

import acr.browser.barebones.activities.BrowserActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;

public class JavaScriptHandler {
	BrowserActivity parentActivity;
	private boolean status = false;
    public JavaScriptHandler(BrowserActivity activity) {
        parentActivity = activity;
    }
    @JavascriptInterface
    public boolean addUrlAsChannel(final String url) {

    	parentActivity.runOnUiThread(new Runnable() {
    		public void run() {
    			synchronized(parentActivity) {
	    			//Log.d("MohuOne", "The url we are trying to add is:> " + url);
	    	    	status = parentActivity.addBookmarkAsChannel(url);
	    	    	//Log.d("MohuOne", "The status after the call is:> " + status);
	    	    	parentActivity.notify();
	    	    	//Log.d("MohuOne", "After notifying the lock objet in the UI Thread.");
    			}
    		}
    	});
    	try {
    		synchronized(parentActivity) {
	    		//Log.d("MohuOne", "addUrlAsChannel(): About to wait on the lock object...");
	    		parentActivity.wait(3000);
				//Log.d("MohuOne", "addUrlAsChannel(): The lock object was notified! Moving forward....");
    		}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Log.e("MohuOne", "addUrlAsChannel(): CRITICAL ERROR! Failed at add channel as thread through interrupt Exception. No channel Added!");
			e.printStackTrace();
			return false;
		}
    	
    	Log.d("MohuOne", "addUrlAsChannel(): Past the lock object. Status is now:> " + status);
    	return status;
    }
    @JavascriptInterface
    public boolean checkIfUrlExists(String url) {
    	Log.d("MohuOne", "Within checkIfUrlExists(): checking the url:> " + url);
    	status = parentActivity.checkIfBookmarkExists(url);
    	Log.d("MohuOne", "Exiting checkIfUrlExists() Javascript Interface exiting.");
    	return status;
    }
    @JavascriptInterface
    public boolean checkIfChannelsDevice() {
    	System.getProperty("os.version");
    	String device = android.os.Build.DEVICE;
    	String product = android.os.Build.PRODUCT;
    	String model = android.os.Build.MODEL;
    	
    	Log.d("MohuOne", "checkIfChannelsDevice(): The propertiest are...");
    	Log.d("MohuOne", "checkIfChannelsDevice(): device:> " + device);
    	Log.d("MohuOne", "checkIfChannelsDevice(): product:> " + product);
    	Log.d("MohuOne", "checkIfChannelsDevice(): model:> " + model);
    	return false;
    }
}
