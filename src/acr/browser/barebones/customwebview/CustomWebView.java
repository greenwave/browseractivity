package acr.browser.barebones.customwebview;

import acr.browser.barebones.activities.BrowserActivity;
import acr.browser.barebones.utilities.CustomHistoryList.HistoryObject;
import acr.browser.barebones.utilities.FinalVariables;
import acr.browser.barebones.utilities.Utils;
import acr.browser.barebones.webviewclasses.CustomChromeClient;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebSettings.RenderPriority;
import android.widget.Toast;

public final class CustomWebView extends WebView {
	static final int API = FinalVariables.API;
	public static boolean showFullScreen;
	final View uBar = BrowserActivity.uBar;
	final Animation slideUp = BrowserActivity.slideUp;
	final Animation slideDown = BrowserActivity.slideDown;
	final boolean DEVICE_HAS_GPS = BrowserActivity.DEVICE_HAS_GPS;
	static Context CONTEXT;
	static String defaultUser;
	public WebSettings settings;
	public boolean faviconOn = false;
	public int activeTabNum = FinalVariables.MAX_TABS;
	//@Mohu Evan Boucher: Added to implement full screen video.
	private CustomChromeClient videoEnabledWebChromeClient;
    private boolean addedJavascriptInterface;
    String currentvideo;
    public int i = 0;
    
    //@Mohu Evan Boucher: FullScreen URLBar: Added to carry out animation.
    /**
     * Boolean for determining if the URLBar is visible. This is altered onKeyPress
     * @Mohu
     */
    private boolean urlBarVisible = true; //Change to true for shown by default.
    /**
     * Determines if a navigation key was pressed. This is so we do not collect history navigation key is hit.
     * @Mohu
     */
    private boolean navWasPressed = false;
    
    /**
     * The history for our custom view.
     * @Mohu Evan Boucher
     */
    private WebBackForwardList view_history;
    
    /**
     * The current index for our history.
     * @Mohu Evan Boucher
     */
    private int view_history_index = 0;
    
    private boolean pageReloaded = false;

	@SuppressLint("NewApi")
	public CustomWebView(Context context) {

		super(context);
		defaultUser = BrowserActivity.defaultUser;
		showFullScreen = BrowserActivity.showFullScreen;
		mGestureDetector = new GestureDetector(context,
				new CustomGestureListener());
		CONTEXT = context;
		settings = getSettings();
		browserInitialization(context);
		settingsInitialization(context);
		//@Mohu Evan Boucher: Added to attempt to autoplay media.
		settings.setMediaPlaybackRequiresUserGesture(false);
		//@Mohu Evan Boucher: Added so that we can save the history and in new views.
		view_history = this.copyBackForwardList();
		view_history_index = view_history.getSize();
		
	}

	@SuppressWarnings("deprecation")
	public void browserInitialization(Context context) {
		setDrawingCacheBackgroundColor(0x00000000);
		setFocusableInTouchMode(true);
		setFocusable(true);
		setAnimationCacheEnabled(false);
		setDrawingCacheEnabled(true);
		setBackgroundColor(context.getResources().getColor(
				android.R.color.white));
		if (API >= 16) {
			getRootView().setBackground(null);
		} else {
			getRootView().setBackgroundDrawable(null);
		}
		setWillNotCacheDrawing(false);
		setAlwaysDrawnWithCacheEnabled(true);
		setScrollbarFadingEnabled(true);
		setSaveEnabled(true);
	}
	
	//@Mohu Evan Boucher: override setWebChromeClient so that it uses our custom version.
	@SuppressLint("SetJavaScriptEnabled")
    public void setWebChromeClient(CustomChromeClient client)
    {
        getSettings().setJavaScriptEnabled(true);

        if (client instanceof CustomChromeClient)
        {
            this.videoEnabledWebChromeClient = (CustomChromeClient) client;
        }

        super.setWebChromeClient(client);
    }

	@SuppressLint("SetJavaScriptEnabled")
	@SuppressWarnings("deprecation")
	public void settingsInitialization(Context context) {
		SharedPreferences preferences = context.getSharedPreferences(
				"settings", 0);
		if(API < 11){
			settings.setLightTouchEnabled(true);
		}
		settings.setDomStorageEnabled(true);
		settings.setAppCacheEnabled(true);
		settings.setAppCachePath(context.getFilesDir().getAbsolutePath()
				+ "/cache");
		settings.setAllowFileAccess(true);
		settings.setDatabaseEnabled(true);
		settings.setDatabasePath(context.getFilesDir().getAbsolutePath()
				+ "/databases");
		settings.setSupportZoom(true);
		settings.setBuiltInZoomControls(true);
		if (API >= 11) {
			settings.setDisplayZoomControls(false);
			settings.setAllowContentAccess(true);
		}

		if (preferences.getBoolean("java", true)) {
			settings.setJavaScriptEnabled(true);
			settings.setJavaScriptCanOpenWindowsAutomatically(true);
		}

		if (API < 14) {
			switch (preferences.getInt("textsize", 2)) {
			case 1:
				settings.setTextSize(WebSettings.TextSize.LARGEST);
				break;
			case 2:
				settings.setTextSize(WebSettings.TextSize.LARGER);
				break;
			case 3:
				settings.setTextSize(WebSettings.TextSize.NORMAL);
				break;
			case 4:
				settings.setTextSize(WebSettings.TextSize.SMALLER);
				break;
			case 5:
				settings.setTextSize(WebSettings.TextSize.SMALLEST);
				break;
			}

		} else {
			switch (preferences.getInt("textsize", 3)) {
			case 1:
				settings.setTextZoom(200);
				break;
			case 2:
				settings.setTextZoom(150);
				break;
			case 3:
				settings.setTextZoom(100);
				break;
			case 4:
				settings.setTextZoom(75);
				break;
			case 5:
				settings.setTextZoom(50);
				break;
			}
		}
		settings.setSupportMultipleWindows(preferences.getBoolean("newwindow",
				true));

		switch (preferences.getInt("enableflash", 2)) {
		case 0:
			break;
		case 1: {
			settings.setPluginState(PluginState.ON_DEMAND);
			break;
		}
		case 2: {
			settings.setPluginState(PluginState.ON);
			break;
		}
		default:
			break;
		}
		if (preferences.getBoolean("passwords", false)) {
			if (API < 18) {
				settings.setSavePassword(true);
			}
			settings.setSaveFormData(true);
		}
		if (API < 18) {
			try {
				settings.setRenderPriority(RenderPriority.HIGH);
			} catch (SecurityException ignored) {

			}
		}
		if (DEVICE_HAS_GPS) {
			settings.setGeolocationEnabled(preferences.getBoolean("location",
					false));
			settings.setGeolocationDatabasePath(context.getFilesDir()
					.getAbsolutePath());
		} else {
			settings.setGeolocationEnabled(false);
		}
		settings.setUseWideViewPort(preferences
				.getBoolean("wideviewport", true));
		settings.setLoadWithOverviewMode(preferences.getBoolean("overviewmode",
				true));

		if (preferences.getBoolean("textreflow", false)) {
			settings.setLayoutAlgorithm(LayoutAlgorithm.NARROW_COLUMNS);
		} else {
			settings.setLayoutAlgorithm(LayoutAlgorithm.NORMAL);
		}

		settings.setBlockNetworkImage(preferences.getBoolean("blockimages",
				false));
		settings.setLoadsImagesAutomatically(true);

		switch (preferences.getInt("agentchoose", 2)) {
		case 1:
			getSettings().setUserAgentString(defaultUser);
			break;
		case 2:
			getSettings().setUserAgentString(FinalVariables.DESKTOP_USER_AGENT);
			break;
		case 3:
			getSettings().setUserAgentString(FinalVariables.MOBILE_USER_AGENT);
			break;
		case 4:
			getSettings().setUserAgentString(
					preferences.getString("userAgentString", defaultUser));
			break;
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(API < 11){
			if(!hasFocus())
				requestFocus();
		}
		mGestureDetector.onTouchEvent(event);
		return super.onTouchEvent(event);
	}

	@Override
	protected void onWindowVisibilityChanged(int visibility) {
		if (API >= 11) {
			setActivated(visibility == View.VISIBLE);
		}
		setEnabled(visibility == View.VISIBLE);
		super.onWindowVisibilityChanged(visibility);
	}

	private final GestureDetector mGestureDetector;

	private class CustomGestureListener extends SimpleOnGestureListener {
		final int SWIPE_THRESHOLD = 100;
		final int SWIPE_VELOCITY_THRESHOLD = 100;
		DisplayMetrics metrics;
		WindowManager wm;
		Display display;
		Point size;
		int width;
		float diffY;
		float diffX;
		boolean first = false;

		@Override
		public boolean onDown(MotionEvent e) {
			first = true;
			return super.onDown(e);
		}

		@Override
		public void onLongPress(MotionEvent e) {
			if (BrowserActivity.currentId != -1) {
				try {
					BrowserActivity.onLongClick();
				} catch (NullPointerException ignored) {
					ignored.printStackTrace();
				}
			}
			super.onLongPress(e);
		}

		@Override
		public boolean onScroll(MotionEvent e1, MotionEvent e2,
				float distanceX, float distanceY) {
			if (showFullScreen && first) {
				first = false;
			}
			return super.onScroll(e1, e2, distanceX, distanceY);
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {

				if (API < 13) {
					metrics = CONTEXT.getResources()
							.getDisplayMetrics();
					width = metrics.widthPixels;
				} else {
					wm = (WindowManager) CONTEXT
							.getSystemService(Context.WINDOW_SERVICE);
					display = wm.getDefaultDisplay();
					size = new Point();
					display.getSize(size);
					width = size.x;
				}

				if ((width - e1.getX() <= width / 15)
						|| (e1.getX() <= width / 15)) {
					diffY = e2.getY() - e1.getY();
					diffX = e2.getX() - e1.getX();
					if (Math.abs(diffX) > Math.abs(diffY)) {
						if (Math.abs(diffX) > SWIPE_THRESHOLD
								&& Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
							if (diffX > 0) {
								BrowserActivity.goBack(CustomWebView.this);
							} else {
								BrowserActivity.goForward(CustomWebView.this);
							}
						}
					}
				}

			} catch (Exception exception) {
				exception.printStackTrace();
			}
			return super.onFling(e1, e2, velocityX, velocityY);
		}
	}
	
	private void addJavascriptInterface()
    {
        if (!addedJavascriptInterface)
        {
            // Add javascript interface to be called when the video ends (must be done before page load)
            addJavascriptInterface(new Object()
            {
                @JavascriptInterface
                public void setCurrentVideo(String video){
                    currentvideo = video;
                }
                @JavascriptInterface 
                public void notifyVideoEnd() // Must match Javascript interface method of VideoEnabledWebChromeClient
                {
                    // This code is not executed in the UI thread, so we must force it to happen
                    new Handler(Looper.getMainLooper()).post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if (videoEnabledWebChromeClient != null)
                            {
                                videoEnabledWebChromeClient.onHideCustomView();
                            }
                        }
                    });
                }
            }, "_VideoEnabledWebView"); // Must match Javascript interface name of VideoEnabledWebChromeClient

            addedJavascriptInterface = true;
        }
    }
	
	@Override
    public void loadUrl(String url)
    {
		Log.d("MohuGoogle", "CustomWebView::loadUrl(): Loading url:> " + url);
		//addJavascriptInterface();
        super.loadUrl(url);
    }
	
	
	/**
	 * This function catches key presses in the webView and does a function.
	 * Currently vertical scrolling is handled here. Refresh button and URL drop down are handled
	 * here as well.
	 * @Mohu Evan Boucher
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d("MohuChannels", "We recieved a keyEvent within the CustomWebView class::> " + keyCode);
		
		switch(keyCode) {
				//@Mohu Evan Boucher: Believe this is where we can add KEYCODE_DPAD_UP and DOWN for scrolling.
				case KeyEvent.KEYCODE_DPAD_UP: {
					Log.d("MohuChannels", "DPAD_UP WAS HIT!!!");
					BrowserActivity.scrollUp();
					//@Mohu: This will not pass the keyPress onward. We capture and take it (always scrolls).
					return true;
				}
				//@Mohu Evan Boucher: Functionality for DPAD down, should scroll down the page.
				case KeyEvent.KEYCODE_DPAD_DOWN: {
					
					Log.d("MohuChannels", "DPAD_DOWN was hit...");
					BrowserActivity.scrollDown();
					//@Mohu: This will not pass the keyPress onward. We capture and take it (always scrolls).
					return true;
				}
				case KeyEvent.KEYCODE_F4: {
					//If the F4 key was hit, then we want to toggle the URL bar.
					if (urlBarVisible) {
						Log.d("MohuChannels", "About to start the animation to slide up for URL bar");
						uBar.startAnimation(slideUp);
						urlBarVisible = false;
					} else {
						Log.d("MohuChannels", "About to show the URL bar. Drop down animation.");
						uBar.startAnimation(slideDown);
						urlBarVisible = true;
					}
					return true;
				}
				/*
				 * This is commented out for latest build (wont be used).
				 * @Mohu Evan Boucher: Catch the rewind button and toggle the url bar.
				 */
				case 140: {
					//Hit the rewind button and toggle URL bar.
					if (urlBarVisible) {
						Log.d("MohuChannels", "About to start the animation to slide up for URL bar");
						uBar.startAnimation(slideUp);
						urlBarVisible = false;
					} else {
						Log.d("MohuChannels", "About to show the URL bar. Drop down animation.");
						uBar.startAnimation(slideDown);
						urlBarVisible = true;
					}
					return true;
				}
				//*/
				/*
				 * Commented out for our latest build (wont be used).
				 * @Mohu Evan Boucher: Capture the play/pause button and refresh the page.
				 */
				case 141: {
					BrowserActivity.currentTab.reload();
					return true;
				}
				//*/
				/*
				case 12: {
					i++;
					switch (i%6) {
					case 1:
						settings.setTextZoom(200);
						break;
					case 2:
						settings.setTextZoom(150);
						break;
					case 3:
						settings.setTextZoom(125);
						break;
					case 4:
						settings.setTextZoom(100);
						break;
					case 5:
						settings.setTextZoom(85);
						break;
					case 6:
						settings.setTextZoom(75);
						break;
					case 7:
						settings.setTextZoom(50);
						break;
					}
					Log.d("MohuOne", "changing textsize. value currently is:> " + i%8);
					
					
				}
				*/
				
		}
		
		return super.onKeyDown(keyCode, event);
		
	}
	/**
	 * @Mohu Created to give functionality for closing the current webView on back press.
	 */
	public void onHideCustomView() {
		
		try {
			videoEnabledWebChromeClient.onHideCustomView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("MohuChannels", "The onHideCustomWebView() method caught an exception:> " + e.getMessage());
		}
		
	}
	
	/**
	 * @Mohu Evan Boucher: Added to catch if the system does not have Internet access.
	 */
	public static boolean isOnline(Context context) {
	    ConnectivityManager cm =
	        (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

	    return cm.getActiveNetworkInfo() != null && 
	       cm.getActiveNetworkInfo().isConnectedOrConnecting();
	}
	/**
	 * Sets the custom history for the webView.
	 * @Mohu Evan Boucher
	 * @return
	 */
	public void setCustomHistory(WebBackForwardList view_history) {
		Log.d("MohuChannels", "setCustomHistory(): Within the setCustomHistory() method in the CustomWebView. old size:> " + view_history.getSize());
		this.view_history = view_history;
		view_history_index = this.view_history.getCurrentIndex();
		Log.d("MohuChannels", "setCustomHistory(): Within the setCustomHistory() method in the CustomWebView. The new size is:> " + view_history.getSize());
		Log.d("MohuChannels", "setCustomHistory(): After being set.The current index is:> " + view_history_index);
	}
	/**
	 * Gets the custom history for the webView.
	 * @Mohu Evan Boucher
	 * @return
	 */
	public WebBackForwardList getCustomHistory() {
		return this.view_history;
	}
	
	/**
	 * Will try to used the saved history to allow us to go back and forward between recreated tabs.
	 * @Mohu Evan Boucher
	 */
	@Override
	public void goBack() {
		//Override the goBack() for now so we can if we have no history saved, pass up to super.
		Log.d("MohuHistory", "goBack(): about to get the temp object.");
		HistoryObject temp = BrowserActivity.goBackHistory();
		//Log.d("MohuHistory", "The temp object is :> " + temp);
		if (temp == null) {
			Log.d("MohuChannels", "About to try and hide the custom view.");
			//videoEnabledWebChromeClient.onHideCustomView();
			Log.d("MohuChannels", "Finished hiding the custom web view.");
			///* Use to toast to say that we cannot go back if null. Above hides the current tab.
			Log.d("MohuHistory", "The previous element was returned null in goBack()");
			Toast.makeText(CONTEXT, "No history to go back to.", Toast.LENGTH_SHORT).show();
			//*/
			return;
		}
		navWasPressed = true;
		this.loadUrl(temp.url);

	}
	
	
	/**
	 * Implementing our own history. Checks if we can go back and then returns the super classes canGoBack() if not.
	 * @Mohu Evan Boucher
	 */
	@Override
	public boolean canGoBack() {
		
		return true; 
	}
	
	@Override
	public void reload() {
		pageReloaded = true;
		super.reload();
	}
	
	/**
	 * Overrides the goForward method to simply change the index of the current custom history.
	 * @Mohu Evan Boucher
	 */
	@Override
	public void goForward() {
		//If our custom history has an idex to go forward, lets move that index forward as well.
		Log.d("MohuHistory", "goForward(): about to get the temp object.");
		HistoryObject temp = BrowserActivity.goForwardHistory();
		//Log.d("MohuHistory", "The temp object is :> " + temp);
		if (temp == null) {
			Log.d("MohuHistory", "The NEXT element was returned null in goBack()");
			Toast.makeText(CONTEXT, "No history to go forward to.", Toast.LENGTH_SHORT).show();
			return;
		}
		navWasPressed = true;
		this.loadUrl(temp.url);
	}
	/**
	 * Overrides the canGoForward to include our custom history.
	 * @Mohu Evan Boucher
	 */
	@Override
	public boolean canGoForward() {
		Log.d("MohuChannels", "canGoForward(): Returing true always.");
		return true; 
	}
	/**
	 * This will determine if the back button was pressed to get the current url. The URL is not added
	 * if the back button was pressed in order to prevent redundancy and history deadlock.
	 * @param url The url to add to the history.
	 */
	public void checkBackPressed(String url) {
		Log.d("MohuHistory", "within checkBackWasPressed(). if statement evaluabed to:> " + !navWasPressed);
		if (!pageReloaded) { //If the page was not reloaded then continue.
			if (!navWasPressed) {
				BrowserActivity.addToHistory(url);
			} 
			navWasPressed = false;
			return;
		}
		Log.d("MohuHistory", "The page was reloaded!");
		pageReloaded = false; //Reset variable since page will be done loading.
		
	}
	
	/**
	 * Simply returns if the history list is dirty or not. 
	 * A dirty list means that the list has a detached head and should be overwritten
	 * @return
	 */
	public boolean checkIfNavWasPressed() {
		//If back has been pressed then the list is dirty and needs to be cleaned before loading a new URL.
		return navWasPressed;
	}
	
}
