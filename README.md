#Lightning Browser for Mohu

This is the open source lightning browser used/edited by Mohu.


Copyright 2014 Anthony Restaino

Lightning Browser

   This Source Code Form is subject to the terms of the 
   Mozilla Public License, v. 2.0. If a copy of the MPL 
   was not distributed with this file, You can obtain one at 

   http://mozilla.org/MPL/2.0/

This software is license under MPL 2.0 license.